package com.example.javierrey.guitarra.Items;
public class SpinnerItenUnoMayor {
    private String NombreItemMayor;
    private int ImagenItemMayor;
    public SpinnerItenUnoMayor(String NombreItemMayor, int ImagenItemMayor) {
        this.NombreItemMayor = NombreItemMayor;
        this.ImagenItemMayor = ImagenItemMayor;
    }
    public String getNombreItemMayor() {
        return NombreItemMayor;
    }
    public int getImagenItemMayor() {
        return ImagenItemMayor;
    }
}
