package com.example.javierrey.guitarra.Actividades;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.example.javierrey.guitarra.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
// debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    // Instanciamos cada boton a un objeto de la clase privada Button, ProgessDialog, ViewFlipper, FirebaseAuth y FirebaseAuth.AuthStateListener.
    private EditText usuario, contraseña;
    private Button iniciar, registrarse;
    private ProgressDialog progressDialog;
    private ViewFlipper viewFlipper;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    int contador =0;
    private FirebaseAuth auth= FirebaseAuth.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // A cada referencia de los botones lo enlazamos con su id
        usuario = (EditText)findViewById(R.id.TXTUser);
        contraseña = (EditText)findViewById(R.id.TXTPass);
        iniciar = (Button)findViewById(R.id.BTNIniciarSesion);
        registrarse = (Button)findViewById(R.id.BTNRegistro);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        iniciar.setOnClickListener(this);
        registrarse.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);

        // Obtenemos una instancia de esta clase llamada getInstance().
        firebaseAuth = FirebaseAuth.getInstance();

        // permite que las presentar imagenes por medio de un array.
        // Es usado usualmente para animación de una interfaz
        viewFlipper = (ViewFlipper)findViewById(R.id.ViewFliper);int imagenes[] = {R.drawable.a, R.drawable.f, R.drawable.c, R.drawable.d, R.drawable.e};
        for (int i = 0; i< imagenes.length; i++){
            fliper(imagenes[i]);
        }

        // preguntamos a la base de datos si tiene a ese usuario
        // si es correcto el usuario y la contraseña entonces accedemos a la interfaz de juegos en la app
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    startActivity(new Intent(MainActivity.this, Juegos.class));
                    finish();
                } else {

                }
            }
        };
    }

    //  Cargamos las imágenes y le damos un tiempo límite de presentacion.
    public void fliper(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // cuando hacemos clic en el botón "iniciar sesión" este realizará las siguientes condicioens..
            case R.id.BTNIniciarSesion:
                // cuando existan campos vacios mostrará una alerta que el campo es necesario
                if (usuario.getText().toString().isEmpty()) {
                    usuario.setError(getResources().getString(R.string.error));
                } else if (contraseña.getText().toString().isEmpty()) {
                    contraseña.setError(getResources().getString(R.string.error));
                } else {
                    if (usuario.getText().toString().isEmpty()) {
                        usuario.setError(getResources().getString(R.string.error));
                    } else if (contraseña.getText().toString().isEmpty()) {
                        contraseña.setError(getResources().getString(R.string.error));
                    /* Si los intentos de escribir la contraseña superan a tres, entonecs se muestra un cuadro de diálogo para poder ir al GMAIL y cambiar la contraseña */
                    } else if (contador >= 3) {
                        //Toast.makeText(MainActivity.this, R.string.excesodecontraseña, Toast.LENGTH_LONG).show();
                        // iniciar.setEnabled(false);
                        contraseña.setError("Exceso de intentos");



                    auth.sendPasswordResetEmail(usuario.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                            }

                        }
                    });
                        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                        alert.setTitle(R.string.alerta).setIcon(R.drawable.common_google_signin_btn_icon_dark);
                        alert.setMessage(R.string.excesodecontraseña);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface alert, int id) {

                                Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.gm")
                                        .setClassName("com.google.android.gm","com.google.android.gm.ConversationListActivityGmail");
                                startActivity(intent);
                                finish();

                            }
                        });

                        alert.show();
                    }






                    else{
                        // comprueba si la clave y correo están escrito de forma correcta
                        final String correo = usuario.getText().toString().trim();
                        String clave = contraseña.getText().toString().trim();
                        // muestra una barra de dialogo y nos dice que se "ESTÁ CONSULTANDO EN LINEA"
                        // si los datos son correctos entonces se mostrará el mensaje "ACCEDIENDO..!"
                        progressDialog.setTitle("CONSULTANDO EN LÍNEA");
                        progressDialog.setMessage("ACCEDIENDO..!");
                        // método para mostrar la barra de dialogo.
                        progressDialog.show();
                        // entonces, comprueba con los datos que están en la base de datos firebase, si estos son iguales se accede
                        // caso contrario saldrá un mensaje "CREDENCIALES ERRONEAS"
                        firebaseAuth.signInWithEmailAndPassword(correo, clave).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Intent intent = new Intent(MainActivity.this, Juegos.class);
                                startActivity(intent);
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(MainActivity.this, "CREDENCIALES ERRONEAS..!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                                contador ++;
                            }
                        });
                    }

                }
                break;
            // accedems a la actividad registro.
            case R.id.BTNRegistro:
                startActivity(new Intent(MainActivity.this, Registro.class));
                finish();
                break;
        }
    }

    // Este método onTouchEvent interpretará los eventos táctiles en esta actividad
    // Cuando estamos usando el teclado del celular y hacemos clic en la pantalla, entonces el teclado se esconde.
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }
    // Inicializamos la bases de datos Firebase
    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }
    // La base de datos se detiene
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
        }
    }

    /* Cuadro de diálogo que muestra una advertencia antes de poder adivinar los acordes musicales. */
    private void Error(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        //mensaje al iniciar la actividad
        dialogo1.setMessage(getResources().getString(R.string.advertencia));
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
            }
        });
        dialogo1.show();
    }

}