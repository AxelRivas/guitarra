package com.example.javierrey.guitarra.Actividades;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.javierrey.guitarra.R;
import java.io.IOException;

// Debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class EscribesNotasMayor extends AppCompatActivity implements View.OnClickListener {

    // Instanciamos cada objeto a la clase privada ImageView, EditText, Button y MediaPlayer.
    private ImageView sonidoMayorUno, sonidoMayorDos, sonidoMayorTres, sonidoMayorCuatro, sonidoMayorCinco, sonidoMayorSeis;
    private EditText respuestaMayorUno, respuestaMayorDos, respuestaMayorTres, respuestaMayorCuatro, respuestaMayorCinco, respuestaMayorSeis;
    private Button comprobarMUno, comprobarMDos, comprobarMTres, comprobarMCuatro, comprobarMCinco, comprobarMSeis;
    private MediaPlayer MPsonidoMuno, MPsonidoMdos, MPsonidoMtres, MpsonidoMcuatro, MPsonidoMcinco, MPsonidoMseis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escribes_notas_mayor);
        setTitle(R.string.escribelanota);
        /* Cada vez que entramos en esta actividad se mostrará un diálogo de como debes escrinir los acordes musicales*/
        Inicio();

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // A cada referecia de ImageView lo enlazamos con su respectivo id.
        sonidoMayorUno = (ImageView)findViewById(R.id.SonidoMayorUno);
        sonidoMayorDos = (ImageView)findViewById(R.id.SonidoMayorDos);
        sonidoMayorTres = (ImageView)findViewById(R.id.SonidoMayorTres);
        sonidoMayorCuatro = (ImageView)findViewById(R.id.SonidoMayorCuatro);
        sonidoMayorCinco = (ImageView)findViewById(R.id.SonidoMayorCinco);
        sonidoMayorSeis = (ImageView)findViewById(R.id.SonidoMayorSeis);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido
        MPsonidoMuno = MediaPlayer.create(this, R.raw.domayor);
        MPsonidoMdos = MediaPlayer.create(this, R.raw.remayor);
        MPsonidoMtres = MediaPlayer.create(this, R.raw.mimayor);
        MpsonidoMcuatro = MediaPlayer.create(this, R.raw.solmayor);
        MPsonidoMcinco = MediaPlayer.create(this, R.raw.lamayor);
        MPsonidoMseis = MediaPlayer.create(this, R.raw.simayor);

        // A cada referencia de los EditText lo enlazamos con su id.
        respuestaMayorUno = (EditText)findViewById(R.id.TXTRespuestaMayorUno);
        respuestaMayorDos = (EditText)findViewById(R.id.TXTRespuestaMayorDos);
        respuestaMayorTres = (EditText)findViewById(R.id.TXTRespuestaMayorTres);
        respuestaMayorCuatro = (EditText)findViewById(R.id.TXTRespuestaMayorCuatro);
        respuestaMayorCinco = (EditText)findViewById(R.id.TXTRespuestaMayorCinco);
        respuestaMayorSeis = (EditText)findViewById(R.id.TXTRespuestaMayorSeis);

        // A cada referencia de los botones lo enlazamos con su id.
        comprobarMUno = (Button)findViewById(R.id.BNTComprobarMayorUno);
        comprobarMDos = (Button)findViewById(R.id.BNTComprobarMayorDos);
        comprobarMTres = (Button)findViewById(R.id.BNTComprobarMayorTres);
        comprobarMCuatro = (Button)findViewById(R.id.BNTComprobarMayorCuatro);
        comprobarMCinco = (Button)findViewById(R.id.BNTComprobarMayorCinco);
        comprobarMSeis = (Button)findViewById(R.id.BNTComprobarMayorSeis);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        sonidoMayorUno.setOnClickListener(this);
        sonidoMayorDos.setOnClickListener(this);
        sonidoMayorTres.setOnClickListener(this);
        sonidoMayorCuatro.setOnClickListener(this);
        sonidoMayorCinco.setOnClickListener(this);
        sonidoMayorSeis.setOnClickListener(this);

        //Al método OnCreate le asociamos el lístener a los botones.
        comprobarMUno.setOnClickListener(this);
        comprobarMDos.setOnClickListener(this);
        comprobarMTres.setOnClickListener(this);
        comprobarMCuatro.setOnClickListener(this);
        comprobarMCinco.setOnClickListener(this);
        comprobarMSeis.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(this, Juegos.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


     /* Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase ene ste caso su único metodo "View".
     Es decir sobrecargamos el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
     en cada caso. */
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            /* Se reproduce el sonido del primer botón */
            case R.id.SonidoMayorUno:
                MPsonidoMuno.start();
                break;

            case R.id.SonidoMayorDos:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMdos.start();
                break;

            case R.id.SonidoMayorTres:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMtres.start();
                break;

            case R.id.SonidoMayorCuatro:
                /* Se reproduce el sonido del primer botón */
                MpsonidoMcuatro.start();
                break;

            case R.id.SonidoMayorCinco:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMcinco.start();
                break;

            case R.id.SonidoMayorSeis:
                /* Se reproduce el sonido del primer botón */
                MPsonidoMseis.start();
                break;


            case R.id.BNTComprobarMayorUno:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorUno.getText().toString().isEmpty()) {
                    respuestaMayorUno.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorUno.getText().toString().equals(getString(R.string.domayor)+" ") ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.domayor)) ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.domayorLPMa)+ " ") ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.domayorLPMa))||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.DdomayorLPMa)+ " ") ||
                        respuestaMayorUno.getText().toString().equals(getString(R.string.DdomayorLPMa))){

                        comprobarMUno.setBackgroundResource(R.drawable.borde_gris_bg);
                        comprobarMDos.setBackgroundResource(R.drawable.button_adivina);
                        comprobarMTres.setBackgroundResource(R.drawable.button_adivina);
                        comprobarMCuatro.setBackgroundResource(R.drawable.button_adivina);
                        comprobarMCinco.setBackgroundResource(R.drawable.button_adivina);
                        comprobarMSeis.setBackgroundResource(R.drawable.button_adivina);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    InCorrecto();
                    respuestaMayorUno.setText("");
                }
                break;

            case R.id.BNTComprobarMayorDos:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorDos.getText().toString().isEmpty()) {
                    respuestaMayorDos.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorDos.getText().toString().equals(getString(R.string.remayor)+" ") ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.remayor)) ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.remayorMPMa)+ " ") ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.remayorMPMa))||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.RremayorMPMa)+ " ") ||
                        respuestaMayorDos.getText().toString().equals(getString(R.string.RremayorMPMa))){

                    comprobarMUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMDos.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarMTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMSeis.setBackgroundResource(R.drawable.button_adivina);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    InCorrecto();
                    respuestaMayorDos.setText("");
                }
                break;

            case R.id.BNTComprobarMayorTres:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorTres.getText().toString().isEmpty()) {
                    respuestaMayorTres.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorTres.getText().toString().equals(getString(R.string.mimayor)+" ")  ||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.mimayorLPMa)+ " ") ||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.mimayorLPMa))||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.MmimayorLPMa)+ " ") ||
                        respuestaMayorTres.getText().toString().equals(getString(R.string.MmimayorLPMa))){

                    comprobarMUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMTres.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarMCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMSeis.setBackgroundResource(R.drawable.button_adivina);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    InCorrecto();
                    respuestaMayorTres.setText("");
                }
                break;

            case R.id.BNTComprobarMayorCuatro:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorCuatro.getText().toString().isEmpty()) {
                    respuestaMayorCuatro.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorCuatro.getText().toString().equals(getString(R.string.solmayor)+" ")  ||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.solmayorLPMa)+ " ") ||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.solmayorLPMa))||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.SsolmayorLPMa)+ " ") ||
                        respuestaMayorCuatro.getText().toString().equals(getString(R.string.SsolmayorLPMa))){

                    comprobarMUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCuatro.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarMCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMSeis.setBackgroundResource(R.drawable.button_adivina);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    InCorrecto();
                    respuestaMayorCuatro.setText("");
                }
                break;

            case R.id.BNTComprobarMayorCinco:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorCinco.getText().toString().isEmpty()) {
                    respuestaMayorCinco.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorCinco.getText().toString().equals(getString(R.string.lamayor)+" ")  ||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.lamayorLPMa)+ " ") ||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.lamayorLPMa))||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.LlamayorLPMa)+ " ") ||
                        respuestaMayorCinco.getText().toString().equals(getString(R.string.LlamayorLPMa))){

                    comprobarMUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCinco.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarMSeis.setBackgroundResource(R.drawable.button_adivina);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    InCorrecto();
                    respuestaMayorCinco.setText("");
                }
                break;

            case R.id.BNTComprobarMayorSeis:
                /* Comprobamos y si lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaMayorSeis.getText().toString().isEmpty()) {
                    respuestaMayorSeis.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaMayorSeis.getText().toString().equals(getString(R.string.simayor)+" ")  ||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.simayorLPMa)+ " ") ||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.simayorLPMa))||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.SsimayorLPMa)+ " ") ||
                        respuestaMayorSeis.getText().toString().equals(getString(R.string.SsimayorLPMa))){

                    comprobarMUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarMSeis.setBackgroundResource(R.drawable.borde_gris_bg);

                    Correcto();
                }else {
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    InCorrecto();
                    respuestaMayorSeis.setText("");
                }
                break;
        }

    }


    private void Inicio(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.advertenciaDOS));
        imageView.setImageResource(R.drawable.ic_advertencia);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

}