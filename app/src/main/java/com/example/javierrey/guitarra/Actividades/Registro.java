package com.example.javierrey.guitarra.Actividades;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.javierrey.guitarra.R;
import com.example.javierrey.guitarra.Modelo.Usuario;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import de.hdodenhof.circleimageview.CircleImageView;


public class Registro extends AppCompatActivity implements View.OnClickListener{
    private EditText nombre, apellido, edad, celular, correo, clave, vclave;
    private Button registrar, IniciarSesion;
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference usuario;
    private CircleImageView perfil;
    private Uri UriResult = null;
    private StorageReference storage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //LLamar el boton de retroceder
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*A cada referencia de los botones lo enlazamos con su id */
        nombre =(EditText)findViewById(R.id.TXTNombres);
        apellido = (EditText)findViewById(R.id.TXTApellidos);
        edad = (EditText)findViewById(R.id.TXTEdad);
        celular = (EditText)findViewById(R.id.TXTCelular);
        correo = (EditText)findViewById(R.id.TXTCorreo);
        clave = (EditText)findViewById(R.id.TXTClave);
        vclave = (EditText)findViewById(R.id.TXTVClave);
        registrar = (Button)findViewById(R.id.BTNRegistrar);
        IniciarSesion = (Button)findViewById(R.id.BTNIniciarS);

        /*  */
        progressDialog = new ProgressDialog(this);

        /* Obtenemos las instancia y referencia de la base de datos tanto del usuario como del almacenamiento */
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        usuario = firebaseDatabase.getReference("Integrantes");
        storage = FirebaseStorage.getInstance().getReference();

        /* Asociamos el listenes a los botones*/
        registrar.setOnClickListener(this);
        IniciarSesion.setOnClickListener(this);

        /* A la referencia de perfil lo enlazamos con su id. */
        perfil = (CircleImageView)findViewById(R.id.CirculoImagenRegistro);

        perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setAspectRatio(1, 1)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(Registro.this);
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BTNRegistrar:
                /* Comprueba que el campo "nombre" esté completo*/
                if (nombre.getText().toString().isEmpty()){
                    nombre.setError(getString(R.string.error));
                    nombre.requestFocus();
                    /* Comprueba que el campo "apellido" esté completo*/
                }else if (apellido.getText().toString().isEmpty()){
                    apellido.setError(getString(R.string.error));
                    apellido.requestFocus();
                    /* Comprueba que el campo "edad" esté completo*/
                }else if (edad.getText().toString().isEmpty()){
                    edad.setError(getString(R.string.error));
                    edad.requestFocus();
                    /* Comprueba que el campo "celular" esté completo*/
                }else if (celular.getText().toString().isEmpty()){
                    celular.setError(getString(R.string.error));
                    celular.requestFocus();
                    /* Comprueba que el campo "completo" esté completo*/
                }else if (correo.getText().toString().isEmpty()){
                    correo.setError(getString(R.string.error));
                    correo.requestFocus();
                    /* Comprueba que el campo "clave" esté completo*/
                }else if (clave.getText().toString().isEmpty()){
                    clave.setError(getString(R.string.error));
                    clave.requestFocus();
                    /* Comprueba que el campo "verificar clave" esté completo*/
                }else if (vclave.getText().toString().isEmpty()) {
                    vclave.setError(getString(R.string.error));
                    vclave.requestFocus();
                /*}else if (CedulaCorrecta(edad.getText().toString())==false) {
                    edad.setError(getString(R.string.cedulacorrecta));
                    cedula.setText("");
                    cedula.requestFocus();*/

                /* Valida que la clave tenga un mínimo de 8 carácteres */
                }else if (validar_clave(clave.getText().toString())==true){
                    clave.setError(getString(R.string.clavevalidar));
                    clave.requestFocus();
                    /* Confirma que la clave ingresada esté correctamente escrita */
                }else if(vclave.getText().toString().equals(clave.getText().toString())==false) {
                    vclave.setError(getString(R.string.comprobarclave) + " " + getString(R.string.password) + " " + getString(R.string.xxx));
                    vclave.requestFocus();
                    /* Comprueba que se ingrese un correo válido */
                }else if (validate_correo(correo.getText().toString())==false) {
                    correo.setError(getString(R.string.correovalido));
                    correo.requestFocus();
                    /* En caso de que no se ubique una imagen nos mandará al método FaltaPerfil que nos dará una aviso para agregar una imagen */
                }else if(UriResult==null){
                    FaltaPerfil();
                }else if (Integer.valueOf(edad.getText().toString())<=0){
                   edad.setError("Edad no puede ser 0");
                }else{


                    /*Va el Código De Registrar */
                    /* "progressDialog nos muestra un indicador de progreso del registro de usuario" */
                    progressDialog.setTitle("REGISTRO");
                    progressDialog.setMessage("REGISTRANDO USUARIO..!");
                    progressDialog.show();

                    /* Autenticamos con firebase mediante el correo y contraseña */
                    firebaseAuth.createUserWithEmailAndPassword(correo.getText().toString(), clave.getText().toString())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult) {
                                    StorageReference file = storage.child("Perfil").child(UriResult.getLastPathSegment());
                                    file.putFile(UriResult).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        /* Enviamos los datos del registro a la base de datos firebase */
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            final Uri Perfil = taskSnapshot.getDownloadUrl();
                                            Usuario User = new Usuario();
                                            User.setNombres(nombre.getText().toString());
                                            User.setApellidos(apellido.getText().toString());
                                            User.setCelular(celular.getText().toString());
                                            User.setEdad(edad.getText().toString() +" " + getString(R.string.años));
                                            User.setCorreo(correo.getText().toString());
                                            User.setClave(clave.getText().toString());
                                            User.setVclave(vclave.getText().toString());
                                            User.setPerfil(Perfil.toString());

                                            usuario.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(User)
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Intent intent = new Intent(Registro.this, Juegos.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    }
                                            });
                                        }

                                    });
                                }
                            });
                }
                break;
                /*  Nos dirige a la pantalla de inicio para poder acceder con el correo y contraseña*/
            case R.id.BTNIniciarS:
                startActivity(new Intent(Registro.this, MainActivity.class));
                finish();
                break;
        }
    }

    /* Creamos este métodos para agregar una imagen dentro del almacenamiento del smartphone*/
    private void FaltaPerfil() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));
        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.Falta));
        imageView.setImageResource(R.drawable.ic_camara_de_fotos);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método para que obtener la imagen y ubicarla en registro */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                UriResult = result.getUri();
                perfil.setImageURI(UriResult);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    /* Método para validar clave */
    public boolean validar_clave(String password){
        String PASSWORD_PATTERN ="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
    /* Método para validar correo */
    public boolean validate_correo(String email){
        String EMAIL_ADDRESS ="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern1 = Pattern.compile(EMAIL_ADDRESS);
        Matcher matcher1 = pattern1.matcher(email);
        return matcher1.matches();
    }

    @Override
    public void onBackPressed(){

    }

}
