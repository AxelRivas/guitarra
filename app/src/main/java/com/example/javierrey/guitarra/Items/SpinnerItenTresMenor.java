package com.example.javierrey.guitarra.Items;

public class SpinnerItenTresMenor {
    private String NombreItemTresMemor;
    private int ImagenItemTresMenor;

    public String getNombreItemTresMemor() {
        return NombreItemTresMemor;
    }

    public int getImagenItemTresMenor() {
        return ImagenItemTresMenor;
    }

    public SpinnerItenTresMenor(String NombreItemTresMemor, int ImagenItemTresMenor) {
        this.NombreItemTresMemor = NombreItemTresMemor;
        this.ImagenItemTresMenor = ImagenItemTresMenor;


        }
    }
