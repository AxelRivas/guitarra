package com.example.javierrey.guitarra.Actividades;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.javierrey.guitarra.R;

import java.io.IOException;

// debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class NotasMayor extends AppCompatActivity implements View.OnClickListener {

    // Instanciamos cada botón a un objeto de la clase privada Button, ImageView y MediaPlayer.
    Button domayor, remayor, mimayor, famayor, solmayor, lamayor, simayor;
    ImageView imaMayores;
    MediaPlayer MPdomayor,MPremayor, MPmimayor, MPfamayor, MPsolmayor, MPlamayor, MPsimayor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas_mayor);
        setTitle(R.string.acordesmayores);

        //LLamar el boton de retroceder
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        // A cada referencia de los botones lo enlazamos con su id.
        domayor = (Button)findViewById(R.id.BTNDomayor);
        remayor = (Button)findViewById(R.id.BTNRemayor);
        mimayor = (Button)findViewById(R.id.BTNMmayor);
        famayor = (Button)findViewById(R.id.BTNFamayor);
        solmayor = (Button)findViewById(R.id.BTNSolmayor);
        lamayor = (Button)findViewById(R.id.BTNLamayor);
        simayor = (Button)findViewById(R.id.BTNSimayor);
        imaMayores = (ImageView)findViewById(R.id.IMGDoMayor);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        domayor.setOnClickListener(this);
        remayor.setOnClickListener(this);
        mimayor.setOnClickListener(this);
        famayor.setOnClickListener(this);
        solmayor.setOnClickListener(this);
        lamayor.setOnClickListener(this);
        simayor.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomayor = MediaPlayer.create(this, R.raw.domayor);
        MPremayor = MediaPlayer.create(this, R.raw.remayor);
        MPmimayor = MediaPlayer.create(this, R.raw.mimayor);
        MPfamayor = MediaPlayer.create(this, R.raw.famayor);
        MPsolmayor = MediaPlayer.create(this, R.raw.solmayor);
        MPlamayor = MediaPlayer.create(this, R.raw.lamayor);
        MPsimayor = MediaPlayer.create(this, R.raw.simayor);



    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }


    //Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase.
    // Es decir sobrecargamos el método el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
    // en cada caso.
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNDomayor:
                // Se reproduce la nota musical
                MPdomayor.start();
                imaMayores.setImageResource(R.drawable.domayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                remayor.setBackgroundResource(R.drawable.button_adivina);
                mimayor.setBackgroundResource(R.drawable.button_adivina);
                famayor.setBackgroundResource(R.drawable.button_adivina);
                solmayor.setBackgroundResource(R.drawable.button_adivina);
                lamayor.setBackgroundResource(R.drawable.button_adivina);
                simayor.setBackgroundResource(R.drawable.button_adivina);
                imaMayores.setBackgroundResource(R.drawable.button_adivina);

                // Si esta sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPremayor.isPlaying()){
                    // Si está sonando el acorde musical "re mayor" entonces se pausa.
                    MPremayor.pause();
                    try {
                        //El acorde musical "re mayor" se devuelve a su estado inical (segundo cero).
                        MPremayor.stop();
                        MPremayor.prepare();
                        MPremayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    MPdomayor.start();
                }else if (MPmimayor.isPlaying()){
                    MPmimayor.pause();
                    try {

                        MPmimayor.stop();
                        MPmimayor.prepare();
                        MPmimayor.seekTo(0);

                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamayor.isPlaying()){
                    MPfamayor.pause();
                    try {
                        MPfamayor.stop();
                        MPfamayor.prepare();
                        MPfamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmayor.isPlaying()) {
                    MPsolmayor.pause();
                    try {
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }




                break;
            case  R.id.BTNRemayor:
                // Se reproduce la nota musical
                MPremayor.start();
                imaMayores.setImageResource(R.drawable.remayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.button_adivina);
                remayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                mimayor.setBackgroundResource(R.drawable.button_adivina);
                famayor.setBackgroundResource(R.drawable.button_adivina);
                solmayor.setBackgroundResource(R.drawable.button_adivina);
                lamayor.setBackgroundResource(R.drawable.button_adivina);
                simayor.setBackgroundResource(R.drawable.button_adivina);
                imaMayores.setBackgroundResource(R.drawable.button_adivina);



                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimayor.isPlaying()){
                    // Si está sonando el acorde musical "mi mayor" entonces se pausa.
                    MPmimayor.pause();
                    try {
                        //El acorde musical "mi mayor" se devuelve a su estado inical (segundo cero).
                        MPmimayor.stop();
                        MPmimayor.prepare();
                        MPmimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamayor.isPlaying()){
                    MPfamayor.pause();
                    try {
                        MPfamayor.stop();
                        MPfamayor.prepare();
                        MPfamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsolmayor.isPlaying()){
                    MPsolmayor.pause();
                    try {
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;


            case R.id.BTNMmayor:
                // Se reproduce la nota musical
                MPmimayor.start();
                imaMayores.setImageResource(R.drawable.mimayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.button_adivina);
                remayor.setBackgroundResource(R.drawable.button_adivina);
                mimayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                famayor.setBackgroundResource(R.drawable.button_adivina);
                solmayor.setBackgroundResource(R.drawable.button_adivina);
                lamayor.setBackgroundResource(R.drawable.button_adivina);
                simayor.setBackgroundResource(R.drawable.button_adivina);
                imaMayores.setBackgroundResource(R.drawable.button_adivina);

                /* Cuanado suena por primera vez el acorde "mi mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamayor.isPlaying()){
                    MPfamayor.pause();
                    // Si está sonando el acorde musical "fa mayor" entonces se pausa.
                    try {
                        //El acorde musical "fa mayor" se devuelve a su estado inical (segundo cero).
                        MPfamayor.stop();
                        MPfamayor.prepare();
                        MPfamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsolmayor.isPlaying()){
                    MPsolmayor.pause();
                    try {
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;


            case R.id.BTNFamayor:
                // Se reproduce la nota musical
                MPfamayor.start();
                imaMayores.setImageResource(R.drawable.famayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.button_adivina);
                remayor.setBackgroundResource(R.drawable.button_adivina);
                mimayor.setBackgroundResource(R.drawable.button_adivina);
                famayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                solmayor.setBackgroundResource(R.drawable.button_adivina);
                lamayor.setBackgroundResource(R.drawable.button_adivina);
                simayor.setBackgroundResource(R.drawable.button_adivina);

                imaMayores.setBackgroundResource(R.drawable.button_adivina);

                /* Cuanado suena por primera vez el acorde "fa mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }

                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsolmayor.isPlaying()) {
                    MPsolmayor.pause();
                    // Si está sonando el acorde musical "sol mayor" entonces se pausa.
                    try {
                        //El acorde musical "sol mayor" se devuelve a su estado inical (segundo cero).
                        MPsolmayor.stop();
                        MPsolmayor.prepare();
                        MPsolmayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPlamayor.isPlaying()){
                    MPlamayor.pause();
                    try {
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimayor.isPlaying()){
                    MPsimayor.pause();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;



            case  R.id.BTNSolmayor:
                // Se reproduce la nota musical
                MPsolmayor.start();
                imaMayores.setImageResource(R.drawable.solmayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.button_adivina);
                remayor.setBackgroundResource(R.drawable.button_adivina);
                mimayor.setBackgroundResource(R.drawable.button_adivina);
                famayor.setBackgroundResource(R.drawable.button_adivina);
                solmayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                lamayor.setBackgroundResource(R.drawable.button_adivina);
                simayor.setBackgroundResource(R.drawable.button_adivina);

                imaMayores.setBackgroundResource(R.drawable.button_adivina);

                /* Cuanado suena por primera vez el acorde "sol mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }


                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }

                MPfamayor.stop();
                try {
                    MPfamayor.stop();
                    MPfamayor.prepare();
                    MPfamayor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPlamayor.isPlaying()){
                    // Si está sonando el acorde musical "la mayor" entonces se pausa.
                    MPlamayor.stop();
                    try {
                        //El acorde musical "la mayor" se devuelve a su estado inical (segundo cero).
                        MPlamayor.stop();
                        MPlamayor.prepare();
                        MPlamayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimayor.isPlaying()){
                    MPsimayor.stop();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }



                break;
            case R.id.BTNLamayor:
                // Se reproduce la nota musical
                MPlamayor.start();
                imaMayores.setImageResource(R.drawable.lamayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.button_adivina);
                remayor.setBackgroundResource(R.drawable.button_adivina);
                mimayor.setBackgroundResource(R.drawable.button_adivina);
                famayor.setBackgroundResource(R.drawable.button_adivina);
                solmayor.setBackgroundResource(R.drawable.button_adivina);
                lamayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                simayor.setBackgroundResource(R.drawable.button_adivina);
                imaMayores.setBackgroundResource(R.drawable.button_adivina);

                /* Cuanado suena por primera vez el acorde "la mayor", entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException ttt){
                    ttt.printStackTrace();

                }
                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException tqq){
                    tqq.printStackTrace();
                }

                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException rww){
                    rww.printStackTrace();
                }


                MPfamayor.stop();
                try {
                    MPfamayor.stop();
                    MPfamayor.prepare();
                    MPfamayor.seekTo(0);
                }catch (IOException bhh){
                    bhh.printStackTrace();
                }

                MPsolmayor.stop();
                try {
                    MPsolmayor.stop();
                    MPsolmayor.prepare();
                    MPsolmayor.seekTo(0);
                }catch (IOException fff){
                    fff.printStackTrace();
                }

                // Si esta sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                if (MPsimayor.isPlaying()){
                    MPsimayor.stop();
                    try {
                        MPsimayor.stop();
                        MPsimayor.prepare();
                        MPsimayor.seekTo(0);
                    }catch (IOException xrr){
                        xrr.printStackTrace();
                    }


                }
                break;


            case R.id.BTNSimayor:
                // Se reproduce la nota musical
                MPsimayor.start();
                imaMayores.setImageResource(R.drawable.simayor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domayor.setBackgroundResource(R.drawable.button_adivina);
                remayor.setBackgroundResource(R.drawable.button_adivina);
                mimayor.setBackgroundResource(R.drawable.button_adivina);
                famayor.setBackgroundResource(R.drawable.button_adivina);
                solmayor.setBackgroundResource(R.drawable.button_adivina);
                lamayor.setBackgroundResource(R.drawable.button_adivina);
                simayor.setBackgroundResource(R.drawable.borde_rojo_bg);
                imaMayores.setBackgroundResource(R.drawable.button_adivina);

                /* Cuanado suena por primera vez si mayor, entonces se detiene cualquier nota que esté sonando */
                MPdomayor.stop();
                try {
                    MPdomayor.stop();
                    MPdomayor.prepare();
                    MPdomayor.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }

                MPremayor.stop();
                try {
                    MPremayor.stop();
                    MPremayor.prepare();
                    MPremayor.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }


                MPmimayor.stop();
                try {
                    MPmimayor.stop();
                    MPmimayor.prepare();
                    MPmimayor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamayor.stop();
                try {
                    MPfamayor.stop();
                    MPfamayor.prepare();
                    MPfamayor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmayor.stop();
                try {
                    MPsolmayor.stop();
                    MPsolmayor.prepare();
                    MPsolmayor.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamayor.stop();
                try {
                    MPlamayor.stop();
                    MPlamayor.prepare();
                    MPlamayor.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;
        }

    }
}

