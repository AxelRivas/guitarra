package com.example.javierrey.guitarra.Modelo;

public class Logros {


    String fecha, escribedomenor, escriberemenor, escribemimenor, escribesolmenor, escribelamenor, escribesimenor;



    public Logros() {
        this.escribedomenor = escribedomenor;
        this.escriberemenor = escriberemenor;
        this.escribemimenor = escribemimenor;
        this.escribesolmenor = escribesolmenor;
        this.escribelamenor = escribelamenor;
        this.escribesimenor = escribesimenor;
        this.fecha = fecha;

    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEscribedomenor() {
        return escribedomenor;
    }

    public void setEscribedomenor(String escribedomenor) {
        this.escribedomenor = escribedomenor;
    }

    public String getEscriberemenor() {
        return escriberemenor;
    }

    public void setEscriberemenor(String escriberemenor) {
        this.escriberemenor = escriberemenor;
    }

    public String getEscribemimenor() {
        return escribemimenor;
    }

    public void setEscribemimenor(String escribemimenor) {
        this.escribemimenor = escribemimenor;
    }


    public String getEscribesolmenor() {
        return escribesolmenor;
    }

    public void setEscribesolmenor(String escribesolmenor) {
        this.escribesolmenor = escribesolmenor;
    }

    public String getEscribelamenor() {
        return escribelamenor;
    }

    public void setEscribelamenor(String escribelamenor) {
        this.escribelamenor = escribelamenor;
    }

    public String getEscribesimenor() {
        return escribesimenor;
    }

    public void setEscribesimenor(String escribesimenor) {
        this.escribesimenor = escribesimenor;
    }
}
