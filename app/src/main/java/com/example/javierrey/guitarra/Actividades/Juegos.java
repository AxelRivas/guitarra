package com.example.javierrey.guitarra.Actividades;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.javierrey.guitarra.R;
import com.google.firebase.auth.FirebaseAuth;
// Debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class Juegos extends AppCompatActivity implements View.OnClickListener{

    // Instanciamos cada objeto de la clase privada Button y FirebaseAuth.
    private Button uno, dos, tres, cuatro, cinco, seis, siete, ocho;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juegos);


        // A cada referencia de los botones lo enlazamos con su id
        uno = (Button)findViewById(R.id.BTNNMenor);
        dos = (Button)findViewById(R.id.BTNNmayor);
        tres = (Button)findViewById(R.id.BTNNMenorSiete);
        cuatro = (Button)findViewById(R.id.BTNNMayorSiete);
        cinco = (Button)findViewById(R.id.BTNCuatro);
        seis = (Button)findViewById(R.id.BTNCinco);
        siete = (Button)findViewById(R.id.BTNSeis);
        ocho= (Button)findViewById(R.id.BTNSiete);

        // Obtenemos una instancia de esta clase llamada getInstance().
        firebaseAuth = FirebaseAuth.getInstance();

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        uno.setOnClickListener(this);
        dos.setOnClickListener(this);
        tres.setOnClickListener(this);
        cuatro.setOnClickListener(this);
        cinco.setOnClickListener(this);
        seis.setOnClickListener(this);
        siete.setOnClickListener(this);
        ocho.setOnClickListener(this);
    }

    //Especificamos el menú de opciones en esta actividad definidos en la actividad XML
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    // Agregamos las acciones cuando seleccionamos un item del menú.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Cerrar_Sesion:
                firebaseAuth.signOut();
                startActivity(new Intent(Juegos.this, MainActivity.class));
                finish();
                break;

            case R.id.Perfil:
                startActivity(new Intent(Juegos.this, Perfil.class));
                break;

            case R.id.Acercade:
                startActivity(new Intent(Juegos.this, AcercaDeDos.class));
        }
        return true;
    }



    //Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase.
    // Es decir sobrecargamos el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
    // en cada caso.

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            // Cuando presionamos el primer botón de esta actividad, entonces nos dirigiremos a la actividad de nombre "
            // Asi continúa con los demás botones.
            case R.id.BTNNMenor:
                startActivity(new Intent(Juegos.this, NotasMenor.class));
                break;

            case R.id.BTNNmayor:
                startActivity(new Intent(Juegos.this, NotasMayor.class));
                break;

            case  R.id.BTNNMenorSiete:
                startActivity(new Intent(Juegos.this, NotasSieteMenor.class));
                break;

            case  R.id.BTNNMayorSiete:
                startActivity(new Intent(Juegos.this, NotasSieteMayor.class));
                break;

            case R.id.BTNCuatro:
                startActivity(new Intent(Juegos.this, AdivinasNotasMenor.class));
                break;

            case R.id.BTNCinco:
                startActivity(new Intent(Juegos.this, EscribesNotasMayor.class));
                break;

            case R.id.BTNSeis:
                startActivity(new Intent(Juegos.this, AdivinaNotasSeptimaMenor.class));
                break;

            case R.id.BTNSiete:
                startActivity(new Intent(Juegos.this, AdivinaNotasSeptimaMayor.class));
                break;

        }
    }

}