package com.example.javierrey.guitarra.Items;

public class SpinnerItenSieteMenor {

    private String NombreItemSieteMenor;
    private int ImagenItemSieteMenor;

    public String getNombreItemSieteMenor() {
        return NombreItemSieteMenor;
    }

    public int getImagenItemSieteMenor() {
        return ImagenItemSieteMenor;

    }

    public SpinnerItenSieteMenor(String NombreItemSieteMenor, int ImagenItemSieteMenor) {
        this.NombreItemSieteMenor = NombreItemSieteMenor;
        this.ImagenItemSieteMenor = ImagenItemSieteMenor;


    }
}
