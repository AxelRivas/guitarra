package com.example.javierrey.guitarra.Items;

public class SpinnerItenDosMayor {
        private String NombreItemDosMayor;
        private int ImagenItemDosMayor;

        public SpinnerItenDosMayor(String NombreItemDosMayor, int ImagenItemDosMayor) {
            this.NombreItemDosMayor = NombreItemDosMayor;
            this.ImagenItemDosMayor = ImagenItemDosMayor;


        }

        public String getNombreItemDosMayor() {
            return NombreItemDosMayor;
        }

        public int getImagenItemDosMayor() {
            return ImagenItemDosMayor;
        }
}