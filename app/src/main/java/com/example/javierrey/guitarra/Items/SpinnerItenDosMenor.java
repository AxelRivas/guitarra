package com.example.javierrey.guitarra.Items;

public class SpinnerItenDosMenor {
    private String NombreItemDosMemor;
    private int ImagenItemDosMenor;
    public SpinnerItenDosMenor(String NombreItemDosMemor, int ImagenItemDosMenor) {
        this.NombreItemDosMemor = NombreItemDosMemor;
        this.ImagenItemDosMenor = ImagenItemDosMenor;
    }
    public String getNombreItemDosMemor() {
        return NombreItemDosMemor;
    }
    public int getImagenItemDosMenor() {
        return ImagenItemDosMenor;
    }
}