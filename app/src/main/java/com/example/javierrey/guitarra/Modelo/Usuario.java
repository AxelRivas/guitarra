package com.example.javierrey.guitarra.Modelo;
public class Usuario {
    /* Instanciamos caja objeto de la clase privada String */
    private String nombres, apellidos, edad, celular, correo, clave, vclave, perfil;
    public Usuario(){
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.celular = celular;
        this.correo = correo;
        this.clave = clave;
        this.vclave = vclave;
        this.perfil = perfil;
    }

    /* Obetenemos el perfil y nos devuelve el perfil */
    public String getPerfil() {
        return perfil;
    }
    /*  */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getEdad() {
        return edad;
    }
    public void setEdad(String edad) {
        this.edad = edad;
    }
    public String getCelular() {
        return celular;
    }
    public void setCelular(String celular) {
        this.celular = celular;
    }
    public String getCorreo() {
        return correo;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }
    public String getVclave() {
        return vclave;
    }
    public void setVclave(String vclave) {
        this.vclave = vclave;
    }
}