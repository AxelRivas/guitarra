package com.example.javierrey.guitarra.Actividades;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javierrey.guitarra.R;
public class AcercaDeDos extends AppCompatActivity {

    // Instanciamos cada objeto a la clase privada TextView y String.
    private TextView Copy;
    private String CopyR = "\u00a9 ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de_dos);


        //A la referencia "copy" lo enlazamos con su id.
        Copy = (TextView)findViewById(R.id.LBLCopy);
        Copy.setText(CopyR + getString(R.string.grupo));
    }



}
