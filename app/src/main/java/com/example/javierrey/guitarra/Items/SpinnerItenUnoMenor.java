package com.example.javierrey.guitarra.Items;
public class SpinnerItenUnoMenor {


    private String NombreItemMemor;
    private int ImagenItemMenor;


    public SpinnerItenUnoMenor(String NombreItemMemor, int ImagenItemMenor) {
        this.NombreItemMemor = NombreItemMemor;
        this.ImagenItemMenor = ImagenItemMenor;
    }
    public String getNombreItemMemor() {
        return NombreItemMemor;
    }
    public int getImagenItemMenor() {
        return ImagenItemMenor;
    }
}
