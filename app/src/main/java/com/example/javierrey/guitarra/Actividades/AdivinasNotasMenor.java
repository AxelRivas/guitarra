package com.example.javierrey.guitarra.Actividades;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javierrey.guitarra.Modelo.Logros;
import com.example.javierrey.guitarra.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;

// Debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class AdivinasNotasMenor extends AppCompatActivity implements View.OnClickListener{

    // Instanciamos cada objeto a la clase privada ImageView, EditText, Button y MediaPlayer.
    private ImageView sonidoUno, sonidoDos, sonidoTres, sonidoCuatro, sonidoCinco, sonidoSeis;
    private EditText respuestaUno, respuestaDos, respuestaTres, respuestaCuatro, respuestaCinco, respuestaSeis;
    private Button comprobarUno, comprobarDos, comprobarTres, comprobarCuatro, comprobarCinco, comprobarSeis;
    private MediaPlayer MPsonidouno, MPsonidodos, MPsonidotres, Mpsonidocuatro, MPsonidocinco, MPsonidoseis;

    /*ICreamos instancias de tipo String y las inicializamos en blanco*/
    private String domenor = "";
    private String remenor = "";
    private String mimenor = "";
    private String solmenor = "";
    private String lamenor = "";
    private String simenor = "";
    private String fecha;

    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference logros_Notas_Menores, users;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivinas_notas_menor);
        setTitle(R.string.escribelanota);


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        /* Cada vez que entramos en esta actividad se mostrará un diálogo de como debes escrinir los acordes musicales*/
        Inicio();

        // A cada referecia de ImageView lo enlazamos con su respectivo id.
        sonidoUno = (ImageView)findViewById(R.id.Sonido);
        sonidoDos = (ImageView)findViewById(R.id.SonidoDos);
        sonidoTres = (ImageView)findViewById(R.id.SonidoTres);
        sonidoCuatro = (ImageView)findViewById(R.id.SonidoCuatro);
        sonidoCinco = (ImageView)findViewById(R.id.SonidoCinco);
        sonidoSeis = (ImageView)findViewById(R.id.SonidoSeis);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido
        MPsonidouno = MediaPlayer.create(this, R.raw.domenor);
        MPsonidodos = MediaPlayer.create(this, R.raw.remenor);
        MPsonidotres = MediaPlayer.create(this, R.raw.mimenor);
        Mpsonidocuatro = MediaPlayer.create(this, R.raw.solmenor);
        MPsonidocinco = MediaPlayer.create(this, R.raw.lamenor);
        MPsonidoseis = MediaPlayer.create(this, R.raw.simenor);

        // A cada referencia de los EditText lo enlazamos con su id.
        respuestaUno = (EditText)findViewById(R.id.TXTRespuesta);
        respuestaDos = (EditText)findViewById(R.id.TXTRespuestaDos);
        respuestaTres = (EditText)findViewById(R.id.TXTRespuestaTres);
        respuestaCuatro = (EditText)findViewById(R.id.TXTRespuestCuatro);
        respuestaCinco = (EditText)findViewById(R.id.TXTRespuestaCinco);
        respuestaSeis = (EditText)findViewById(R.id.TXTRespuestaSeis);

        // A cada referencia de los botones lo enlazamos con su id.
        comprobarUno = (Button)findViewById(R.id.BNTComprobarUno);
        comprobarDos = (Button)findViewById(R.id.BNTComprobarDos);
        comprobarTres = (Button)findViewById(R.id.BNTComprobarTres);
        comprobarCuatro = (Button)findViewById(R.id.BNTComprobarCuatro);
        comprobarCinco = (Button)findViewById(R.id.BNTComprobarCinco);
        comprobarSeis = (Button)findViewById(R.id.BNTComprobarSeis);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        sonidoUno.setOnClickListener(this);
        sonidoDos.setOnClickListener(this);
        sonidoTres.setOnClickListener(this);
        sonidoCuatro.setOnClickListener(this);
        sonidoCinco.setOnClickListener(this);
        sonidoSeis.setOnClickListener(this);

        //Al método OnCreate le asociamos el lístener a los botones.
        comprobarUno.setOnClickListener(this);
        comprobarDos.setOnClickListener(this);
        comprobarTres.setOnClickListener(this);
        comprobarCuatro.setOnClickListener(this);
        comprobarCinco.setOnClickListener(this);
        comprobarSeis.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        users = firebaseDatabase.getReference();
        logros_Notas_Menores = firebaseDatabase.getReference("LOGROS MENORES");
        progressDialog = new ProgressDialog(this);


        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss a");
                                String dateString = sdf.format(date);
                                fecha = dateString;
                            }
                        });
                    }
                } catch (InterruptedException e) {

                }
            }
        };
        t.start();
    }




    // Este método onTouchEvent interpreta los eventos táctiles de una actividad
    // En este caso oculta el teclado.
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }

    //Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase ene ste caso su único metodo "View".
    // Es decir sobrecargamos el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
    // en cada caso.

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            /* Se reproduce el sonido del primer botón */
            case R.id.Sonido:
                MPsonidouno.start();
                break;

            case R.id.SonidoDos:
                /* Se reproduce el sonido del segundo botón */
                MPsonidodos.start();
                break;

            case R.id.SonidoTres:
                /* Se reproduce el sonido del tercer botón */
                MPsonidotres.start();
                break;

            case R.id.SonidoCuatro:
                /* Se reproduce el sonido del cuarto botón */
                Mpsonidocuatro.start();
                break;

            case R.id.SonidoCinco:
                /* Se reproduce el sonido del quinto botón */
                MPsonidocinco.start();
                break;

            case R.id.SonidoSeis:
                /* Se reproduce el sonido del sexto botón */
                MPsonidoseis.start();
                break;


            case R.id.BNTComprobarUno:
                /* Cuando comprobamos y lo que esta en el editText está vacio entonces nos envirá una advertencia */
                if (respuestaUno.getText().toString().isEmpty()) {
                    respuestaUno.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaUno.getText().toString().equals(getString(R.string.domenor)+" ") ||
                        respuestaUno.getText().toString().equals(getString(R.string.domenor)) ||
                        respuestaUno.getText().toString().equals(getString(R.string.domenorLPMe)+ " ") ||
                        respuestaUno.getText().toString().equals(getString(R.string.domenorLPMe))||
                        respuestaUno.getText().toString().equals(getString(R.string.DdomenorLPMe)+ " ") ||
                        respuestaUno.getText().toString().equals(getString(R.string.DdomenorLPMe))){

                    comprobarUno.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarSeis.setBackgroundResource(R.drawable.button_adivina);

                    if (domenor==""){
                        domenor = "CORRECTO: "+respuestaUno.getText().toString();
                    }else {
                        domenor +=  " - " + "CORRECTO: "+respuestaUno.getText().toString();
                    }
                    comprobarUno.setEnabled(false);
                    respuestaUno.setText("CORRECTO");
                    respuestaUno.setEnabled(false);
                Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (domenor==""){
                        domenor = "INCORRECTO: "+ respuestaUno.getText().toString();
                    }else {
                        domenor +=  " - " + "INCORRECTO: "+respuestaUno.getText().toString();
                    }
                    InCorrecto();
                    respuestaUno.setText("");
                }
                break;

            case R.id.BNTComprobarDos:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaDos.getText().toString().isEmpty()) {
                    respuestaDos.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaDos.getText().toString().equals(getString(R.string.remenor)+" ")  ||
                        respuestaDos.getText().toString().equals(getString(R.string.remenor)) ||
                        respuestaDos.getText().toString().equals(getString(R.string.remenorLPMe)+ " ") ||
                        respuestaDos.getText().toString().equals(getString(R.string.remenorLPMe))||
                        respuestaDos.getText().toString().equals(getString(R.string.RremenorLPMe)+ " ") ||
                        respuestaDos.getText().toString().equals(getString(R.string.RremenorLPMe))){

                    comprobarUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarDos.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarSeis.setBackgroundResource(R.drawable.button_adivina);

                    if (remenor==""){
                        remenor= "CORECTO: "+respuestaDos.getText().toString();
                    }else {
                        remenor+= " - "+ "CORRECTO: " +respuestaDos.getText().toString();
                    }

                    respuestaDos.setText("CORRECTO");
                    respuestaDos.setEnabled(false);
                    comprobarDos.setEnabled(false);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (remenor==""){
                        remenor= "INCORECTO: "+respuestaDos.getText().toString();
                    }else {
                        remenor+= " - "+ "INCORRECTO: " +respuestaDos.getText().toString();
                    }

                    InCorrecto();
                    respuestaDos.setText("");
                }
                break;

            case R.id.BNTComprobarTres:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaTres.getText().toString().isEmpty()) {
                    respuestaTres.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaTres.getText().toString().equals(getString(R.string.mimenor)+" ")||
                        respuestaTres.getText().toString().equals(getString(R.string.mimenor)) ||
                        respuestaTres.getText().toString().equals(getString(R.string.mimenorLPMe)+ " ") ||
                        respuestaTres.getText().toString().equals(getString(R.string.mimenorLPMe))||
                        respuestaTres.getText().toString().equals(getString(R.string.MmimenorLPMe)+ " ") ||
                        respuestaTres.getText().toString().equals(getString(R.string.MmimenorLPMe))){

                    comprobarUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarTres.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarSeis.setBackgroundResource(R.drawable.button_adivina);

                    if (mimenor==""){
                        mimenor= "CORECTO: "+respuestaTres.getText().toString();
                    }else {
                        mimenor+= " - "+ "CORRECTO: " +respuestaTres.getText().toString();
                    }

                    respuestaTres.setText("CORRECTO");
                    respuestaTres.setEnabled(false);
                    comprobarTres.setEnabled(false);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */

                    if (mimenor==""){
                        mimenor= "INCORECTO: "+respuestaTres.getText().toString();
                    }else {
                        mimenor+= " - "+ "INCORRECTO: " +respuestaTres.getText().toString();
                    }

                    InCorrecto();
                    respuestaTres.setText("");
                }
                break;

            case R.id.BNTComprobarCuatro:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaCuatro.getText().toString().isEmpty()) {
                    respuestaCuatro.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCuatro.getText().toString().equals(getString(R.string.solmenor)+" ")||
                        respuestaCuatro.getText().toString().equals(getString(R.string.solmenor)) ||
                        respuestaCuatro.getText().toString().equals(getString(R.string.solmenorLPMe)+ " ") ||
                        respuestaCuatro.getText().toString().equals(getString(R.string.solmenorLPMe))||
                        respuestaCuatro.getText().toString().equals(getString(R.string.SsolmenorLPMe)+ " ") ||
                        respuestaCuatro.getText().toString().equals(getString(R.string.SsolmenorLPMe))){

                    comprobarUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCuatro.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarSeis.setBackgroundResource(R.drawable.button_adivina);
                    if (solmenor==""){
                        solmenor= "CORECTO: "+respuestaCuatro.getText().toString();
                    }else {
                        solmenor+= " - "+ "CORRECTO: " +respuestaCuatro.getText().toString();
                    }

                    respuestaCuatro.setText("CORRECTO");
                    respuestaCuatro.setEnabled(false);
                    comprobarCuatro.setEnabled(false);
                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (solmenor==""){
                        solmenor= "INCORECTO: "+respuestaCuatro.getText().toString();
                    }else {
                        solmenor+= " - "+ "INCORRECTO: " +respuestaCuatro.getText().toString();
                    }


                    InCorrecto();
                    respuestaCuatro.setText("");
                }
                break;

            case R.id.BNTComprobarCinco:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaCinco.getText().toString().isEmpty()) {
                    respuestaCinco.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaCinco.getText().toString().equals(getString(R.string.lamenor)+" ") ||
                        respuestaCinco.getText().toString().equals(getString(R.string.lamenor)) ||
                        respuestaCinco.getText().toString().equals(getString(R.string.lamenorLPMe)+ " ") ||
                        respuestaCinco.getText().toString().equals(getString(R.string.lamenorLPMe))||
                        respuestaCinco.getText().toString().equals(getString(R.string.LlamenorLPMe)+ " ") ||
                        respuestaCinco.getText().toString().equals(getString(R.string.LlamenorLPMe))){

                    comprobarUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCinco.setBackgroundResource(R.drawable.borde_gris_bg);
                    comprobarSeis.setBackgroundResource(R.drawable.button_adivina);

                    if (lamenor==""){
                        lamenor= "CORECTO: "+respuestaCinco.getText().toString();
                    }else {
                        lamenor+= " - "+ "CORRECTO: " +respuestaCinco.getText().toString();
                    }

                    respuestaCinco.setText("CORRECTO");
                    respuestaCinco.setEnabled(false);
                    comprobarCinco.setEnabled(false);

                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (lamenor==""){
                        lamenor= "INCORECTO: "+respuestaCinco.getText().toString();
                    }else {
                        lamenor+= " - "+ "INCORRECTO: " +respuestaCinco.getText().toString();
                    }

                    InCorrecto();
                    respuestaCinco.setText("");
                }
                break;

            case R.id.BNTComprobarSeis:
                /* Cuando comprobamos y lo que esta en el editText  está vacio entonces nos envirá una advertencia */
                if (respuestaSeis.getText().toString().isEmpty()) {
                    respuestaSeis.setError(getString(R.string.escribirNota));
                    /* Condición para escribir la nota */
                }else if (respuestaSeis.getText().toString().equals(getString(R.string.simenor)+" ") ||
                        respuestaSeis.getText().toString().equals(getString(R.string.simenor)) ||
                        respuestaSeis.getText().toString().equals(getString(R.string.simenorLPMe)+ " ") ||
                        respuestaSeis.getText().toString().equals(getString(R.string.simenorLPMe))||
                        respuestaSeis.getText().toString().equals(getString(R.string.SsimenorLPMe)+ " ") ||
                        respuestaSeis.getText().toString().equals(getString(R.string.SsimenorLPMe))){

                    comprobarUno.setBackgroundResource(R.drawable.button_adivina);
                    comprobarDos.setBackgroundResource(R.drawable.button_adivina);
                    comprobarTres.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCuatro.setBackgroundResource(R.drawable.button_adivina);
                    comprobarCinco.setBackgroundResource(R.drawable.button_adivina);
                    comprobarSeis.setBackgroundResource(R.drawable.borde_gris_bg);

                    if (simenor==""){
                        simenor= "CORECTO: "+respuestaSeis.getText().toString();
                    }else {
                        simenor+= " - "+ "CORRECTO: " +respuestaSeis.getText().toString();
                    }

                    respuestaSeis.setText("CORRECTO");
                    respuestaSeis.setEnabled(false);
                    comprobarSeis.setEnabled(false);


                    Correcto();
                }else{
                    /* Si no se cumplen las condiciones anteriores entonces nos mostrará un mensaje que está incorrecto */
                    if (simenor==""){
                        simenor= "INCORECTO: "+respuestaSeis.getText().toString();
                    }else {
                        simenor+= " - "+ "INCORRECTO: " +respuestaSeis.getText().toString();
                    }

                    InCorrecto();
                    respuestaSeis.setText("");
                }
                break;
        }
    }

/*
//  Cuadro de diálogo que muestra una advertencia antes de poder adivinar los acordes musicales.
    private void Inicio(){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
        dialogo1.setMessage(getResources().getString(R.string.advertencia));
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
            }
        });
        dialogo1.show();
    }
 */
    private void Inicio(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.advertencia));
        imageView.setImageResource(R.drawable.ic_advertencia);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            //if (respuestaDos.getText().toString().equals("CORRECTO") && respuestaUno.getText().toString().equals("CORRECTO")) {

//            Log.e("DoMenor", domenor);
//            Log.e("DoMenor", remenor);
//            Log.e("DoMenor", mimenor);
//            Log.e("DoMenor", solmenor);
//            Log.e("DoMenor", lamenor);
//            Log.e("DoMenor", simenor);

            progressDialog.setTitle("GUARDANDO DATOS");
            progressDialog.setMessage("ESPERE POR FAVOR..!");
            progressDialog.show();

            /* Creamos objetos de la clases logros para poder hacer uso de esa clase */
            Logros logros = new Logros();
            logros.setFecha(fecha);
            logros.setEscribedomenor(domenor);
            logros.setEscriberemenor(remenor);
            logros.setEscribemimenor(mimenor);
            logros.setEscribesolmenor(solmenor);
            logros.setEscribelamenor(lamenor);
            logros.setEscribesimenor(simenor);


            logros_Notas_Menores.child(FirebaseAuth.getInstance().getUid()).setValue(logros).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    progressDialog.hide();
                    startActivity(new Intent(AdivinasNotasMenor.this, Juegos.class));
                    finish();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });

            //}else {

            //}
        }
        return super.onOptionsItemSelected(item);
    }

}