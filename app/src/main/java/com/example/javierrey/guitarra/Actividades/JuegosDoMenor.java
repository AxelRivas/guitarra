package com.example.javierrey.guitarra.Actividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.javierrey.guitarra.R;

public class JuegosDoMenor extends AppCompatActivity implements View.OnClickListener {

    Button NMenor, NMayor, NMenorSiete, NMayorSiete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juegos_do_menor);
        NMenor = (Button)findViewById(R.id.BTNNMenor);
        NMayor = (Button)findViewById(R.id.BTNNmayor);
        NMenorSiete = (Button)findViewById(R.id.BTNNMenorSiete);
        NMayorSiete = (Button)findViewById(R.id.BTNNMayorSiete);

        NMenor.setOnClickListener(this);
        NMayor.setOnClickListener(this);

    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNNMenor:
                startActivity(new Intent(JuegosDoMenor.this, NotasMenor.class));
                break;
            case R.id.BTNNmayor:
                startActivity(new Intent(JuegosDoMenor.this, NotasMayor.class));
                break;

        }
    }
}
