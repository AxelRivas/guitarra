package com.example.javierrey.guitarra.Actividades;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.URLUtil;
import android.widget.EditText;
import com.example.javierrey.guitarra.Modelo.Usuario;
import com.example.javierrey.guitarra.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
public class Perfil extends AppCompatActivity {

    // Declaramos las variables a usar en esta actividad en este caso  para obtener los datos del usuario.
    // Instanciamos cada objeto de las clases privadas EditText, FirebaseDatabase, DatabaseReference, String...
    private EditText nombres, apellidos, edad, celular, correo;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference usuario;
    private String userId;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseAuth firebaseAuth;
    private CircleImageView Fto;
    private StorageReference storage;
    private Uri UriResult = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.verperfil);


        // A cada referencia lo enlazamos con su id.
        setContentView(R.layout.activity_perfil);
        nombres = (EditText)findViewById(R.id.TXTNombresR);
        apellidos = (EditText)findViewById(R.id.TXTApellidosR);
        edad = (EditText)findViewById(R.id.TXTEdadR);
        celular = (EditText)findViewById(R.id.TXTCelularR);
        correo = (EditText)findViewById(R.id.TXTCorreoR);
        Fto = (CircleImageView) findViewById(R.id.CirculoImagenRegistroR);

        /* Obrenemos instancia de la base de datos */
        firebaseDatabase = FirebaseDatabase.getInstance();

        /* Obtenemos instancia para autenticar el usuario*/
        firebaseAuth = FirebaseAuth.getInstance();

        /* Obtenemos instancia y referencias del almacenamiento de firebase */
        storage = FirebaseStorage.getInstance().getReference();

        /* Obetenemos de la base de datos el suusario actual. */
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        usuario.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Datos(dataSnapshot);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        // método para autenticar al usuario en la base de datos firebase
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            }
        };
    }

    // Con la instancia DataSnapShot obtendremos los datos del usuario que se encuentra logeado.
    private void Datos(DataSnapshot dataSnapshot){
        for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
            Usuario usuarioLogin = new Usuario();

            // obetenemos los datos que queremos que se muestren en el perfil del usuario, nos devuelve los datos contenidos en esta instancia
            usuarioLogin.setNombres(dataSnapshot1.child(userId).getValue(Usuario.class).getNombres());
            usuarioLogin.setApellidos(dataSnapshot1.child(userId).getValue(Usuario.class).getApellidos());
            usuarioLogin.setEdad(dataSnapshot1.child(userId).getValue(Usuario.class).getEdad());
            usuarioLogin.setCelular(dataSnapshot1.child(userId).getValue(Usuario.class).getCelular());
            usuarioLogin.setCorreo(dataSnapshot1.child(userId).getValue(Usuario.class).getCorreo());
            usuarioLogin.setPerfil(dataSnapshot1.child(userId).getValue(Usuario.class).getPerfil());


            /*  Como ya obtenemos los datos entonces los mostramos*/
            nombres.setText(usuarioLogin.getNombres());
            apellidos.setText(usuarioLogin.getApellidos());
            edad.setText(usuarioLogin.getEdad());
            celular.setText(usuarioLogin.getCelular());
            correo.setText(usuarioLogin.getCorreo());
            /* Para obtener la dirección del perfil */
            String url = String.valueOf(usuarioLogin.getPerfil());
            if (URLUtil.isValidUrl(url)){
                /* Con la librería Picasso, nos pide un contexto y cargamos la imagen donde la queremos ubicar (Fto)*/
                Picasso.with(getApplicationContext()).load(Uri.parse(url)).into(Fto);
            }
        }
    }
}
