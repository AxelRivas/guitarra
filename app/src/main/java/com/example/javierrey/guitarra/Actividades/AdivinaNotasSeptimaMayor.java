package com.example.javierrey.guitarra.Actividades;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.javierrey.guitarra.Adapter.AdapterSpinerCincoMayor;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerCinco;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerCuatro;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerCuatroMayor;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerDos;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerDosMayor;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerSeis;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerSeisMayor;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerSiete;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerSieteMayor;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerTres;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerTresMayor;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerUno;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerUnoMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenCincoMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenCincoMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenCuatroMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenCuatroMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenDosMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenDosMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenSeisMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenSeisMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenSieteMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenSieteMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenTresMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenTresMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenUnoMayor;
import com.example.javierrey.guitarra.Items.SpinnerItenUnoMenor;
import com.example.javierrey.guitarra.R;

import java.util.ArrayList;
/* Implementamos la  interfaz OnClickListener y hacemos las importaciones de la interfaz*/
public class AdivinaNotasSeptimaMayor extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout LLunomayor, LLdosmayor, LLtresmayor, LLcuatromayor, LLcincomayor, LLseismayor, LLsietemayor;



    /* Necesitamos una lista que deseamos que muestre el spinner. Agarramos los métodos de la clase creada (SpinnerItenUnoMayor)
también llamamos a esta clase los métodos de la clase AdapterSpinnerUnoMayor para poder implementarlos.
Asi mismo con los demás...
*/
    private ArrayList<SpinnerItenUnoMayor> spinnerItenUnoMayor;
    private AdapterSpinnerUnoMayor adapterSpinnerUnoMayor;

    private ArrayList<SpinnerItenDosMayor> spinnerItenDosMayor;
    private AdapterSpinnerDosMayor adapterSpinnerDosMayor;

    private ArrayList<SpinnerItenTresMayor> spinnerItenTresMayor;
    private AdapterSpinnerTresMayor adapterSpinnerTresMayor;

    private ArrayList<SpinnerItenCuatroMayor> spinnerItenCuatroMayor;
    private AdapterSpinnerCuatroMayor adapterSpinnerCuatroMayor;

    private ArrayList<SpinnerItenCincoMayor> spinnerItenCincoMayor;
    private AdapterSpinerCincoMayor adapterSpinnerCincoMayor;

    private ArrayList<SpinnerItenSeisMayor> spinnerItenSeisMayor;
    private AdapterSpinnerSeisMayor adapterSpinnerSeisMayor;

    private ArrayList<SpinnerItenSieteMayor> spinnerItenSieteMayor;
    private AdapterSpinnerSieteMayor adapterSpinnerSieteMayor;


    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMayor, spinnerDosMayor, spinnerTresMayor, spinnerCuatroMayor,
            spinnerCincoMayor, spinnerSeisMayor, spinnerSieteMayor;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMayor, dosMayor, tresMayor, cuatroMayor, cincoMayor, seisMayor, sieteMayor;
    private MediaPlayer MPunoMayor, MPdosMayor, MPtresMayor, MPcuatroMayor, MPcincoMayor, MPseisMayor, MPsieteMayor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivina_notas_septima_mayor);
        setTitle(getString(R.string.escuchayelije));


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        LLunomayor = (LinearLayout)findViewById(R.id.LLUnomenor);
        LLdosmayor = (LinearLayout)findViewById(R.id.LLDosmenor);
        LLtresmayor = (LinearLayout)findViewById(R.id.LLTresmenor);
        LLcuatromayor = (LinearLayout)findViewById(R.id.LLCuatromenor);
        LLcincomayor = (LinearLayout)findViewById(R.id.LLCincomenor);
        LLseismayor = (LinearLayout)findViewById(R.id.LLSeismenor);
        LLsietemayor = (LinearLayout)findViewById(R.id.LLSietemenor);

        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMayor = MediaPlayer.create(this, R.raw.do7);
        MPdosMayor = MediaPlayer.create(this, R.raw.re7);
        MPtresMayor = MediaPlayer.create(this, R.raw.mi7);
        MPcuatroMayor = MediaPlayer.create(this, R.raw.fa7);
        MPcincoMayor = MediaPlayer.create(this, R.raw.sol7);
        MPseisMayor = MediaPlayer.create(this, R.raw.la7);
        MPsieteMayor = MediaPlayer.create(this, R.raw.si7);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMayor = (ImageView)findViewById(R.id.SPImagenUnoMayor);
        dosMayor = (ImageView)findViewById(R.id.SPImagenDosMayor);
        tresMayor = (ImageView)findViewById(R.id.SPImagenTresMayor);
        cuatroMayor = (ImageView)findViewById(R.id.SPImagenCuatroMayor);
        cincoMayor = (ImageView)findViewById(R.id.SPImagenCincoMayor);
        seisMayor = (ImageView)findViewById(R.id.SPImagenSeisMayor);
        sieteMayor = (ImageView)findViewById(R.id.SPImagenSieteMayor);

        //Asociamos el lístener a los botones.
        unoMayor.setOnClickListener(this);
        dosMayor.setOnClickListener(this);
        tresMayor.setOnClickListener(this);
        cuatroMayor.setOnClickListener(this);
        cincoMayor.setOnClickListener(this);
        seisMayor.setOnClickListener(this);
        sieteMayor.setOnClickListener(this);

        LLunomayor.setOnClickListener(this);
        LLdosmayor.setOnClickListener(this);
        LLtresmayor.setOnClickListener(this);
        LLcuatromayor.setOnClickListener(this);
        LLcincomayor.setOnClickListener(this);
        LLseismayor.setOnClickListener(this);


        SpinnerUnoMayorPersonalizado();
        SpinnerDosMayorPersonalizado();
        SpinnerTresMayorPersonalizado();
        SpinnerCuatroMayorPersonalizado();
        SpinnerCincoMayorPersonalizado();
        SpinnerSeisMayorPersonalizado();
        SpinnerSieteMayorPersonalizado();


        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMayor = (Spinner) findViewById(R.id.SPUnoMayor);
        spinnerDosMayor = (Spinner)findViewById(R.id.SPDosMayor);
        spinnerTresMayor = (Spinner)findViewById(R.id.SPTresMayor);
        spinnerCuatroMayor = (Spinner)findViewById(R.id.SPCuatroMayor);
        spinnerCincoMayor = (Spinner)findViewById(R.id.SPCincoMayor);
        spinnerSeisMayor = (Spinner)findViewById(R.id.SPSeisMayor);
        spinnerSieteMayor = (Spinner)findViewById(R.id.SPSieteMayor);


        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenUnoMayor clickedItem = (SpinnerItenUnoMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMayor();
                if (clickedCountryName.equals(getString(R.string.simayor7))) {
                    LLunomayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);

                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {
                } else {
                    LLunomayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenDosMayor clickedItem = (SpinnerItenDosMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemDosMayor();
                if (clickedCountryName.equals(getString(R.string.lamayor7))) {
                    LLdosmayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {
                    LLdosmayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenTresMayor clickedItem = (SpinnerItenTresMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemTresMayor();
                if (clickedCountryName.equals(getString(R.string.famayor7))) {
                    LLtresmayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {
                    LLtresmayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCuatroMayor clickedItem = (SpinnerItenCuatroMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCuatroMayor();
                if (clickedCountryName.equals(getString(R.string.solmayor7))) {
                    LLcuatromayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {
                    LLcuatromayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCincoMayor clickedItem = (SpinnerItenCincoMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCincoMayor();
                if (clickedCountryName.equals(getString(R.string.mimayor7))) {
                    LLcincomayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {
                    LLcincomayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSeisMayor clickedItem = (SpinnerItenSeisMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSeisMayor();
                if (clickedCountryName.equals(getString(R.string.remayor7))) {
                    LLseismayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {
                    LLseismayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMayor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSieteMayor clickedItem = (SpinnerItenSieteMayor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSieteMayor();
                if (clickedCountryName.equals(getString(R.string.domayor7))) {
                    LLsietemayor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                } else if (clickedCountryName.equals(getString(R.string.opcion))) {

                } else {
                    LLsietemayor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMayor = (Spinner) findViewById(R.id.SPUnoMayor);
        spinnerDosMayor = (Spinner)findViewById(R.id.SPDosMayor);
        spinnerTresMayor = (Spinner)findViewById(R.id.SPTresMayor);
        spinnerCuatroMayor = (Spinner)findViewById(R.id.SPCuatroMayor);
        spinnerCincoMayor = (Spinner)findViewById(R.id.SPCincoMayor);
        spinnerSeisMayor = (Spinner)findViewById(R.id.SPSeisMayor);
        spinnerSieteMayor = (Spinner)findViewById(R.id.SPSieteMayor);

        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUnoMayor = new AdapterSpinnerUnoMayor(this, spinnerItenUnoMayor);
        adapterSpinnerDosMayor = new AdapterSpinnerDosMayor(this, spinnerItenDosMayor);
        adapterSpinnerTresMayor = new AdapterSpinnerTresMayor(this, spinnerItenTresMayor);
        adapterSpinnerCuatroMayor = new AdapterSpinnerCuatroMayor(this, spinnerItenCuatroMayor);
        adapterSpinnerCincoMayor = new AdapterSpinerCincoMayor(this, spinnerItenCincoMayor);
        adapterSpinnerSeisMayor = new AdapterSpinnerSeisMayor(this, spinnerItenSeisMayor);
        adapterSpinnerSieteMayor = new AdapterSpinnerSieteMayor(this, spinnerItenSieteMayor);

        /* Seteamos el adaptador */
        spinnerUnoMayor.setAdapter(adapterSpinnerUnoMayor);
        spinnerDosMayor.setAdapter(adapterSpinnerDosMayor);
        spinnerTresMayor.setAdapter(adapterSpinnerTresMayor);
        spinnerCuatroMayor.setAdapter(adapterSpinnerCuatroMayor);
        spinnerCincoMayor.setAdapter(adapterSpinnerCincoMayor);
        spinnerSeisMayor.setAdapter(adapterSpinnerSeisMayor);
        spinnerSieteMayor.setAdapter(adapterSpinnerSieteMayor);
       }

    /* Creamos un arreglo para el primer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMayorPersonalizado () {
        spinnerItenUnoMayor = new ArrayList<>();
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMayor.add(new SpinnerItenUnoMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));

    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMayorPersonalizado () {
        spinnerItenDosMayor = new ArrayList<>();
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenDosMayor.add(new SpinnerItenDosMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));


    }


    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMayorPersonalizado () {
        spinnerItenTresMayor = new ArrayList<>();
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenTresMayor.add(new SpinnerItenTresMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMayorPersonalizado () {
        spinnerItenCuatroMayor = new ArrayList<>();
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMayor.add(new SpinnerItenCuatroMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMayorPersonalizado () {
        spinnerItenCincoMayor = new ArrayList<>();
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMayor.add(new SpinnerItenCincoMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));


    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMayorPersonalizado () {
        spinnerItenSeisMayor = new ArrayList<>();
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMayor.add(new SpinnerItenSeisMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMayorPersonalizado () {
        spinnerItenSieteMayor = new ArrayList<>();
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.domayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.mimayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.remayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.famayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.lamayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.solmayor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMayor.add(new SpinnerItenSieteMayor(getString(R.string.simayor7), R.drawable.ic_nota_dp));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            /* Se reproduce el sonido del primer botón */
            case R.id.SPImagenUnoMayor:
                MPsieteMayor.start();
                break;

            /* Se reproduce el sonido del segundo botón */
            case R.id.SPImagenDosMayor:
                MPseisMayor.start();
                break;

            /* Se reproduce el sonido del tercer botón */
            case R.id.SPImagenTresMayor:
                MPcincoMayor.start();
                break;

            /* Se reproduce el sonido del cuarto botón */
            case R.id.SPImagenCuatroMayor:
                MPcuatroMayor.start();
                break;

            /* Se reproduce el sonido del quinto botón */
            case R.id.SPImagenCincoMayor:
                MPtresMayor.start();
                break;

            /* Se reproduce el sonido del sexto botón */
            case R.id.SPImagenSeisMayor:
                MPdosMayor.start();
                break;

            /* Se reproduce el sonido del septimo botón */
            case R.id.SPImagenSieteMayor:
                MPunoMayor.start();
                break;

        }
    }


    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(this, Juegos.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}