package com.example.javierrey.guitarra.Items;

public class SpinnerItenCincoMenor {

    private String NombreItemCincoMenor;
    private int ImagenItemCincoMenor;

    public String getNombreItemCincoMenor() {
        return NombreItemCincoMenor;
    }

    public int getImagenItemCincoMenor() {
        return ImagenItemCincoMenor;

    }

    public SpinnerItenCincoMenor(String NombreItemCincoMenor, int ImagenItemCincoMenor) {
        this.NombreItemCincoMenor = NombreItemCincoMenor;
        this.ImagenItemCincoMenor = ImagenItemCincoMenor;


    }
}
