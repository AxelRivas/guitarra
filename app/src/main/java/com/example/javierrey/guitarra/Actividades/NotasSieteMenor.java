package com.example.javierrey.guitarra.Actividades;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.example.javierrey.guitarra.R;
import java.io.IOException;

// debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class NotasSieteMenor extends AppCompatActivity implements View.OnClickListener {

    // Instanciamos cada botón a un objeto de la clase privada Button, ImageView y MediaPlayer.
    Button DoMenorSiete, ReMenorSiete, MiMenorSiete, FaMenorSiete, SolMenorSiete, LaMenorSiete, SiMenorSiete;
    ImageView TodasMenorSiete;
    MediaPlayer MPdomenor7, MPremenor7, MPmimenor7, MPfamenor7, MPsolmenor7, MPlamenor7, MPsimenor7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas_siete_menor);
        setTitle(R.string.acordesmenores7);

        //LLamar el boton de retroceder
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // A cada referencia de los botones lo enlazamos con su id.
        DoMenorSiete = (Button)findViewById(R.id.BTNDomenorS);
        ReMenorSiete = (Button)findViewById(R.id.BTNRemenorS);
        MiMenorSiete = (Button)findViewById(R.id.BTNMimenorS);
        FaMenorSiete = (Button)findViewById(R.id.BTNFamenorS);
        SolMenorSiete = (Button)findViewById(R.id.BTNSolmenorS);
        LaMenorSiete = (Button)findViewById(R.id.BTNLamenorS);
        SiMenorSiete = (Button)findViewById(R.id.BTNSimenorS);
        TodasMenorSiete = (ImageView)findViewById(R.id.IMGDoMenorSiete);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        DoMenorSiete.setOnClickListener(this);
        ReMenorSiete.setOnClickListener(this);
        MiMenorSiete.setOnClickListener(this);
        FaMenorSiete.setOnClickListener(this);
        SolMenorSiete.setOnClickListener(this);
        LaMenorSiete.setOnClickListener(this);
        SiMenorSiete.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomenor7 = MediaPlayer.create(this, R.raw.domenor7);
        MPremenor7 = MediaPlayer.create(this, R.raw.remenor7);
        MPmimenor7 = MediaPlayer.create(this, R.raw.mimenor7);
        MPfamenor7 = MediaPlayer.create(this, R.raw.famenor7);
        MPsolmenor7 = MediaPlayer.create(this, R.raw.solmenor7);
        MPlamenor7 = MediaPlayer.create(this, R.raw.lamenor7);
        MPsimenor7 = MediaPlayer.create(this, R.raw.simenor7);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase.
    // Es decir sobrecargamos el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
    // en cada caso.
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNDomenorS:
                MPdomenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.domenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                ReMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                MiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                FaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SolMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                LaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPremenor7.isPlaying()){
                    MPremenor7.pause();
                    try {
                        MPremenor7.stop();
                        MPremenor7.prepare();
                        MPremenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    MPdomenor7.start();
                }else if (MPmimenor7.isPlaying()){
                    MPmimenor7.pause();
                    try {
                            MPmimenor7.stop();
                            MPmimenor7.prepare();
                            MPmimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
            }else if (MPfamenor7.isPlaying()){
                    MPfamenor7.pause();
                    try {
                        MPfamenor7.stop();
                        MPfamenor7.prepare();
                        MPfamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmenor7.isPlaying()) {
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case  R.id.BTNRemenorS:
                MPremenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.remenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                ReMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                MiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                FaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SolMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                LaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando la  nota musical "do menor 7", entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                    try {
                        MPdomenor7.stop();
                        MPdomenor7.prepare();
                        MPdomenor7.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimenor7.isPlaying()){
                    MPmimenor7.pause();
                    try {
                        MPmimenor7.stop();
                        MPmimenor7.prepare();
                        MPmimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPfamenor7.isPlaying()){
                        MPfamenor7.pause();
                        try {
                            MPfamenor7.stop();
                            MPfamenor7.prepare();
                            MPfamenor7.seekTo(0);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                }else if (MPsolmenor7.isPlaying()){
                        MPsolmenor7.pause();
                        try {
                            MPsolmenor7.stop();
                            MPsolmenor7.prepare();
                            MPsolmenor7.seekTo(0);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                }else if (MPlamenor7.isPlaying()){
                        MPlamenor7.pause();
                        try {
                            MPlamenor7.stop();
                            MPlamenor7.prepare();
                            MPlamenor7.seekTo(0);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                }else if (MPsimenor7.isPlaying()){
                        MPsimenor7.pause();
                        try {
                            MPsimenor7.stop();
                            MPsimenor7.prepare();
                            MPsimenor7.seekTo(0);
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                }
                break;
            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNMimenorS:
                MPmimenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.mimenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                ReMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                MiMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                FaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SolMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                LaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando las  notas musicales "do menor 7" o "re menor 7", entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamenor7.isPlaying()){
                    MPfamenor7.pause();
                    try {
                        MPfamenor7.stop();
                        MPfamenor7.prepare();
                        MPfamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPsolmenor7.isPlaying()){
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNFamenorS:
                MPfamenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.famenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                ReMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                MiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                FaMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                SolMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                LaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7" o "mi menor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }
                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                if (MPsolmenor7.isPlaying())
                {
                    MPsolmenor7.pause();
                    try {
                        MPsolmenor7.stop();
                        MPsolmenor7.prepare();
                        MPsolmenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPlamenor7.isPlaying()){
                    MPlamenor7.pause();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPsimenor7.isPlaying()){
                    MPsimenor7.pause();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case  R.id.BTNSolmenorS:
                MPsolmenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.solmenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                ReMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                MiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                FaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SolMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                LaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7", "mi menor 7" o "fa menor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }
                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }
                MPfamenor7.stop();
                try {
                    MPfamenor7.stop();
                    MPfamenor7.prepare();
                    MPfamenor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPlamenor7.isPlaying()){
                    MPlamenor7.stop();
                    try {
                        MPlamenor7.stop();
                        MPlamenor7.prepare();
                        MPlamenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                } else if (MPsimenor7.isPlaying()){
                    MPsimenor7.stop();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNLamenorS:
                MPlamenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.lamenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                ReMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                MiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                FaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SolMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                LaMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                SiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7", "mi menor 7", "fa menor 7" o "sol menor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();
                }
                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }
                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }
                MPfamenor7.stop();
                try {
                    MPfamenor7.stop();
                    MPfamenor7.prepare();
                    MPfamenor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }
                MPsolmenor7.stop();
                try {
                    MPsolmenor7.stop();
                    MPsolmenor7.prepare();
                    MPsolmenor7.seekTo(0);
                }catch (IOException ff){
                    ff.printStackTrace();
                }

                // Si está sonando la nota musical "si menor 7", entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                if (MPsimenor7.isPlaying()){
                    MPsimenor7.stop();
                    try {
                        MPsimenor7.stop();
                        MPsimenor7.prepare();
                        MPsimenor7.seekTo(0);
                    }catch (IOException xr){
                        xr.printStackTrace();
                    }
                }
                break;

            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNSimenorS:
                MPsimenor7.start();
                TodasMenorSiete.setImageResource(R.drawable.simenor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                DoMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                ReMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                MiMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                FaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SolMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                LaMenorSiete.setBackgroundResource(R.drawable.button_adivina);
                SiMenorSiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                TodasMenorSiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do menor 7", "re menor 7", "mi menor 7", "fa menor 7", "sol menor 7" o "la menor 7",
                // entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor7.stop();
                try {
                    MPdomenor7.stop();
                    MPdomenor7.prepare();
                    MPdomenor7.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();
                }

                MPremenor7.stop();
                try {
                    MPremenor7.stop();
                    MPremenor7.prepare();
                    MPremenor7.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }

                MPmimenor7.stop();
                try {
                    MPmimenor7.stop();
                    MPmimenor7.prepare();
                    MPmimenor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamenor7.stop();
                try {
                    MPfamenor7.stop();
                    MPfamenor7.prepare();
                    MPfamenor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmenor7.stop();
                try {
                    MPsolmenor7.stop();
                    MPsolmenor7.prepare();
                    MPsolmenor7.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamenor7.stop();
                try {
                    MPlamenor7.stop();
                    MPlamenor7.prepare();
                    MPlamenor7.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;
        }
    }
}