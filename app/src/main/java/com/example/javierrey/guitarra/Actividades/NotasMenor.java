package com.example.javierrey.guitarra.Actividades;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.javierrey.guitarra.R;

import java.io.IOException;

// debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class NotasMenor extends AppCompatActivity implements View.OnClickListener {

    // Instanciamos cada botón a un objeto de la clase privada Button, ImageView y MediaPlayer.
    Button domenor, remenor, mimenor, famenor, solmenor, lamenor, simenor;
    ImageView imagennotamenor;
    MediaPlayer MPdomenor, MPremenor, MPmimenor, MPfamenor, MPsolmenor, MPlamenor, MPsimenor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas_menor);
        setTitle(R.string.acordesmenores);

        //LLamar el boton de retroceder
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // A cada referencia de los botones lo enlazamos con su id.
        domenor = (Button)findViewById(R.id.BTNDomenor);
        remenor = (Button)findViewById(R.id.BTNRemenor);
        mimenor = (Button)findViewById(R.id.BTNMimenor);
        famenor = (Button)findViewById(R.id.BTNFamenor);
        solmenor = (Button)findViewById(R.id.BTNSolmenor);
        lamenor = (Button)findViewById(R.id.BTNLamenor);
        simenor = (Button)findViewById(R.id.BTNSimenor);
       imagennotamenor = (ImageView)findViewById(R.id.IMGDoMenor);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        domenor.setOnClickListener(this);
        remenor.setOnClickListener(this);
        mimenor.setOnClickListener(this);
        famenor.setOnClickListener(this);
        solmenor.setOnClickListener(this);
        lamenor.setOnClickListener(this);
        simenor.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomenor = MediaPlayer.create(this, R.raw.domenor);
        MPremenor = MediaPlayer.create(this, R.raw.remenor);
        MPmimenor = MediaPlayer.create(this, R.raw.mimenor);
        MPfamenor = MediaPlayer.create(this, R.raw.famenor);
        MPsolmenor = MediaPlayer.create(this, R.raw.solmenor);
        MPlamenor = MediaPlayer.create(this, R.raw.lamenor);
        MPsimenor = MediaPlayer.create(this, R.raw.simenor);

    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase.
    // Es decir sobrecargamos el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
    // en cada caso.
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BTNDomenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPdomenor.start();
                imagennotamenor.setImageResource(R.drawable.domenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                remenor.setBackgroundResource(R.drawable.button_adivina);
                mimenor.setBackgroundResource(R.drawable.button_adivina);
                famenor.setBackgroundResource(R.drawable.button_adivina);
                solmenor.setBackgroundResource(R.drawable.button_adivina);
                lamenor.setBackgroundResource(R.drawable.button_adivina);
                simenor.setBackgroundResource(R.drawable.button_adivina);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPremenor.isPlaying()){
                    // Si está sonando el acorde musical "re menor" entonces se pausa.
                    MPremenor.pause();
                    try {
                        //El acorde musical "re menor" se devuelve a su estado inical (se inicia desde cero).
                        MPremenor.stop();
                        MPremenor.prepare();
                        MPremenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    MPdomenor.start();
                }else if (MPmimenor.isPlaying()){
                    MPmimenor.pause();
                    try {

                        MPmimenor.stop();
                        MPmimenor.prepare();
                        MPmimenor.seekTo(0);

                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamenor.isPlaying()){
                    MPfamenor.pause();
                    try {
                        MPfamenor.stop();
                        MPfamenor.prepare();
                        MPfamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmenor.isPlaying()) {
                    MPsolmenor.pause();
                    try {
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }




                break;
            case  R.id.BTNRemenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPremenor.start();
                imagennotamenor.setImageResource(R.drawable.remenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domenor.setBackgroundResource(R.drawable.button_adivina);
                remenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                mimenor.setBackgroundResource(R.drawable.button_adivina);
                famenor.setBackgroundResource(R.drawable.button_adivina);
                solmenor.setBackgroundResource(R.drawable.button_adivina);
                lamenor.setBackgroundResource(R.drawable.button_adivina);
                simenor.setBackgroundResource(R.drawable.button_adivina);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimenor.isPlaying()){
                    // Si está sonando el acorde musical "mi menor" entonces se pausa.
                    MPmimenor.pause();
                    try {
                        //El acorde musical "mi menor" se devuelve a su estado inical (se inicia desde cero).
                        MPmimenor.stop();
                        MPmimenor.prepare();
                        MPmimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamenor.isPlaying()){
                    MPfamenor.pause();
                    try {
                        MPfamenor.stop();
                        MPfamenor.prepare();
                        MPfamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsolmenor.isPlaying()){
                    MPsolmenor.pause();
                    try {
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }


                break;


            case R.id.BTNMimenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPmimenor.start();
                imagennotamenor.setImageResource(R.drawable.mimenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domenor.setBackgroundResource(R.drawable.button_adivina);
                remenor.setBackgroundResource(R.drawable.button_adivina);
                mimenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                famenor.setBackgroundResource(R.drawable.button_adivina);
                solmenor.setBackgroundResource(R.drawable.button_adivina);
                lamenor.setBackgroundResource(R.drawable.button_adivina);
                simenor.setBackgroundResource(R.drawable.button_adivina);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamenor.isPlaying()){
                    // Si está sonando el acorde musical "fa menor" entonces se pausa.
                    MPfamenor.pause();
                    try {
                        //El acorde musical "fa menor" se devuelve a su estado inical (se inicia desde cero).
                        MPfamenor.stop();
                        MPfamenor.prepare();
                        MPfamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsolmenor.isPlaying()){
                    MPsolmenor.pause();
                    try {
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }





                break;
            case R.id.BTNFamenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPfamenor.start();
                imagennotamenor.setImageResource(R.drawable.famenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domenor.setBackgroundResource(R.drawable.button_adivina);
                remenor.setBackgroundResource(R.drawable.button_adivina);
                mimenor.setBackgroundResource(R.drawable.button_adivina);
                famenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                solmenor.setBackgroundResource(R.drawable.button_adivina);
                lamenor.setBackgroundResource(R.drawable.button_adivina);
                simenor.setBackgroundResource(R.drawable.button_adivina);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();
                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }

                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsolmenor.isPlaying()) {
                    // Si está sonando el acorde musical "sol menor" entonces se pausa.
                    MPsolmenor.pause();
                    try {
                        //El acorde musical "sol menor" se devuelve a su estado inical (se inicia desde cero).
                        MPsolmenor.stop();
                        MPsolmenor.prepare();
                        MPsolmenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPlamenor.isPlaying()){
                    MPlamenor.pause();
                    try {
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimenor.isPlaying()){
                    MPsimenor.pause();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;



            case  R.id.BTNSolmenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPsolmenor.start();
                imagennotamenor.setImageResource(R.drawable.solmenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domenor.setBackgroundResource(R.drawable.button_adivina);
                remenor.setBackgroundResource(R.drawable.button_adivina);
                mimenor.setBackgroundResource(R.drawable.button_adivina);
                famenor.setBackgroundResource(R.drawable.button_adivina);
                solmenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                lamenor.setBackgroundResource(R.drawable.button_adivina);
                simenor.setBackgroundResource(R.drawable.button_adivina);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();
                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }


                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }

                MPfamenor.stop();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPlamenor.isPlaying()){
                    // Si está sonando el acorde musical "la menor" entonces se pausa.
                    MPlamenor.stop();
                    try {
                        //El acorde musical "la menor" se devuelve a su estado inical (se inicia desde cero).
                        MPlamenor.stop();
                        MPlamenor.prepare();
                        MPlamenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimenor.isPlaying()){
                    MPsimenor.stop();
                    try {
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }



                break;
            case R.id.BTNLamenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPlamenor.start();
                imagennotamenor.setImageResource(R.drawable.lamenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domenor.setBackgroundResource(R.drawable.button_adivina);
                remenor.setBackgroundResource(R.drawable.button_adivina);
                mimenor.setBackgroundResource(R.drawable.button_adivina);
                famenor.setBackgroundResource(R.drawable.button_adivina);
                solmenor.setBackgroundResource(R.drawable.button_adivina);
                lamenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                simenor.setBackgroundResource(R.drawable.button_adivina);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical  se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }
                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }

                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }


                MPfamenor.stop();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmenor.stop();
                try {
                    MPsolmenor.stop();
                    MPsolmenor.prepare();
                    MPsolmenor.seekTo(0);
                }catch (IOException ff){
                    ff.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsimenor.isPlaying()){
                    MPsimenor.stop();
                    // Si está sonando el acorde musical "si menor" entonces se pausa.
                    try {
                        //El acorde musical "si menor" se devuelve a su estado inical (se inicia desde cero).
                        MPsimenor.stop();
                        MPsimenor.prepare();
                        MPsimenor.seekTo(0);
                    }catch (IOException xr){
                        xr.printStackTrace();
                    }
                }
                break;


            case R.id.BTNSimenor:
                // Se reproduce la nota musical y se muestra la imagen de la nota
                MPsimenor.start();
                imagennotamenor.setImageResource(R.drawable.simenor);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo
                domenor.setBackgroundResource(R.drawable.button_adivina);
                remenor.setBackgroundResource(R.drawable.button_adivina);
                mimenor.setBackgroundResource(R.drawable.button_adivina);
                famenor.setBackgroundResource(R.drawable.button_adivina);
                solmenor.setBackgroundResource(R.drawable.button_adivina);
                lamenor.setBackgroundResource(R.drawable.button_adivina);
                simenor.setBackgroundResource(R.drawable.borde_rojo_bg);
                imagennotamenor.setBackgroundResource(R.drawable.button_adivina);


                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical se devuelve a su estado inical (se inicia desde cero).
                MPdomenor.stop();
                try {
                    MPdomenor.stop();
                    MPdomenor.prepare();
                    MPdomenor.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }

                MPremenor.stop();
                try {
                    MPremenor.stop();
                    MPremenor.prepare();
                    MPremenor.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }


                MPmimenor.stop();
                try {
                    MPmimenor.stop();
                    MPmimenor.prepare();
                    MPmimenor.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamenor.stop();
                try {
                    MPfamenor.stop();
                    MPfamenor.prepare();
                    MPfamenor.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmenor.stop();
                try {
                    MPsolmenor.stop();
                    MPsolmenor.prepare();
                    MPsolmenor.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamenor.stop();
                try {
                    MPlamenor.stop();
                    MPlamenor.prepare();
                    MPlamenor.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;
        }
    }
}
