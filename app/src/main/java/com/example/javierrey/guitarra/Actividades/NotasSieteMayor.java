package com.example.javierrey.guitarra.Actividades;

import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.javierrey.guitarra.R;

import java.io.IOException;

// debemos implementar la interfaz OnClickListener y hacemos las importaciones de la interfaz.
public class NotasSieteMayor extends AppCompatActivity implements View.OnClickListener {

    // Instanciamos cada botón a un objeto de la clase privada Button, ImageView y MediaPlayer.
        Button domayorsiete, remayorsiete, mimayorsiete, famayorsiete, solmayorsiete, lamayorsiete, simayorsiete;
        ImageView todomayorsiete;
        MediaPlayer MPdomayor7, MPremayor7, MPmimayor7, MPfamayor7, MPsolmayor7, MPlamayor7, MPsimayor7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas_siete_mayor);
        setTitle(R.string.acordesmayores7);


        //LLamar el boton de retroceder
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // A cada referencia de los botones lo enlazamos con su id.
        domayorsiete = (Button)findViewById(R.id.BTNDomayorSiete);
        remayorsiete = (Button)findViewById(R.id.BTNRemayorSiete);
        mimayorsiete = (Button)findViewById(R.id.BTNMimayorSiete);
        famayorsiete = (Button)findViewById(R.id.BTNFamayorSiete);
        solmayorsiete = (Button)findViewById(R.id.BTNSolayorSiete);
        lamayorsiete = (Button)findViewById(R.id.BTNLamayorSiete);
        simayorsiete = (Button)findViewById(R.id.BTNSimayorSiete);
        todomayorsiete = (ImageView)findViewById(R.id.IMGDoMayorSiete);

        //Dentro de este método OnCreate asociamos el lístener a los botones.
        domayorsiete.setOnClickListener(this);
        remayorsiete.setOnClickListener(this);
        mimayorsiete.setOnClickListener(this);
        famayorsiete.setOnClickListener(this);
        solmayorsiete.setOnClickListener(this);
        lamayorsiete.setOnClickListener(this);
        simayorsiete.setOnClickListener(this);

        //  A cada referencia de MediaPlayer lo enlazamos con su respectivo sonido.
        MPdomayor7 = MediaPlayer.create(this, R.raw.do7);
        MPremayor7 = MediaPlayer.create(this, R.raw.re7);
        MPmimayor7 = MediaPlayer.create(this, R.raw.mi7);
        MPfamayor7 = MediaPlayer.create(this, R.raw.fa7);
        MPsolmayor7 = MediaPlayer.create(this, R.raw.sol7);
        MPlamayor7 = MediaPlayer.create(this, R.raw.la7);
        MPsimayor7 = MediaPlayer.create(this, R.raw.si7);

    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }


    //Para que esta interfaz funciones debemos implementar sus métodos dentro de nuestra clase.
    // Es decir sobrecargamos el método OnClick y dentro de el distinguimos que botón fué presionado y realizamos la acción asociada
    // en cada caso.
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            // Se reproduce la nota musical y se muestra la imagen de la nota.
            case R.id.BTNDomayorSiete:
                MPdomayor7.start();
                todomayorsiete.setImageResource(R.drawable.domayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                remayorsiete.setBackgroundResource(R.drawable.button_adivina);
                mimayorsiete.setBackgroundResource(R.drawable.button_adivina);
                famayorsiete.setBackgroundResource(R.drawable.button_adivina);
                solmayorsiete.setBackgroundResource(R.drawable.button_adivina);
                lamayorsiete.setBackgroundResource(R.drawable.button_adivina);
                simayorsiete.setBackgroundResource(R.drawable.button_adivina);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPremayor7.isPlaying()){
                    // Si está sonando el acorde musical "re mayor 7" entonces se pausa.
                    MPremayor7.pause();
                    try {
                        //El acorde musical "re mayor 7" se devuelve a su estado inical (se inicia desde cero).
                        MPremayor7.stop();
                        MPremayor7.prepare();
                        MPremayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                    MPdomayor7.start();
                }else if (MPmimayor7.isPlaying()){
                    MPmimayor7.pause();
                    try {

                        MPmimayor7.stop();
                        MPmimayor7.prepare();
                        MPmimayor7.seekTo(0);

                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamayor7.isPlaying()){
                    MPfamayor7.pause();
                    try {
                        MPfamayor7.stop();
                        MPfamayor7.prepare();
                        MPfamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsolmayor7.isPlaying()) {
                    MPsolmayor7.pause();
                    try {
                        MPsolmayor7.stop();
                        MPsolmayor7.prepare();
                        MPsolmayor7.seekTo(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (MPlamayor7.isPlaying()){
                    MPlamayor7.pause();
                    try {
                        MPlamayor7.stop();
                        MPlamayor7.prepare();
                        MPlamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsimayor7.isPlaying()){
                    MPsimayor7.pause();
                    try {
                        MPsimayor7.stop();
                        MPsimayor7.prepare();
                        MPsimayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }




                break;
            case  R.id.BTNRemayorSiete:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPremayor7.start();
                todomayorsiete.setImageResource(R.drawable.remayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.button_adivina);
                remayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                mimayorsiete.setBackgroundResource(R.drawable.button_adivina);
                famayorsiete.setBackgroundResource(R.drawable.button_adivina);
                solmayorsiete.setBackgroundResource(R.drawable.button_adivina);
                lamayorsiete.setBackgroundResource(R.drawable.button_adivina);
                simayorsiete.setBackgroundResource(R.drawable.button_adivina);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);

                // Si está sonando la  nota musical "do mayor 7", entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomayor7.stop();
                try {
                    MPdomayor7.stop();
                    MPdomayor7.prepare();
                    MPdomayor7.seekTo(0);


                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPmimayor7.isPlaying()){
                    // Si está sonando el acorde musical "mi mayor 7" entonces se pausa.
                    MPmimayor7.pause();
                    try {
                        MPmimayor7.stop();
                        MPmimayor7.prepare();
                        MPmimayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPfamayor7.isPlaying()){
                    MPfamayor7.pause();
                    try {
                        MPfamayor7.stop();
                        MPfamayor7.prepare();
                        MPfamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPsolmayor7.isPlaying()){
                    MPsolmayor7.pause();
                    try {
                        MPsolmayor7.stop();
                        MPsolmayor7.prepare();
                        MPsolmayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPlamayor7.isPlaying()){
                    MPlamayor7.pause();
                    try {
                        MPlamayor7.stop();
                        MPlamayor7.prepare();
                        MPlamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor7.isPlaying()){
                    MPsimayor7.pause();
                    try {
                        MPsimayor7.stop();
                        MPsimayor7.prepare();
                        MPsimayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }


                break;


            case R.id.BTNMimayorSiete:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPmimayor7.start();
                todomayorsiete.setImageResource(R.drawable.mimayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.button_adivina);
                remayorsiete.setBackgroundResource(R.drawable.button_adivina);
                mimayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                famayorsiete.setBackgroundResource(R.drawable.button_adivina);
                solmayorsiete.setBackgroundResource(R.drawable.button_adivina);
                lamayorsiete.setBackgroundResource(R.drawable.button_adivina);
                simayorsiete.setBackgroundResource(R.drawable.button_adivina);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando las  notas musicales "do mayor 7" o "re mayor 7", entonces se detiene  y da paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomayor7.stop();
                try {
                    MPdomayor7.stop();
                    MPdomayor7.prepare();
                    MPdomayor7.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();

                }

                MPremayor7.stop();
                try {
                    MPremayor7.stop();
                    MPremayor7.prepare();
                    MPremayor7.seekTo(0);
                }catch (IOException r){
                    r.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPfamayor7.isPlaying()){
                    // Si está sonando el acorde musical "fa mayor 7" entonces se pausa.
                    MPfamayor7.pause();
                    try {
                        //El acorde musical "fa mayor 7" se devuelve a su estado inical (se inicia desde cero).
                        MPfamayor7.stop();
                        MPfamayor7.prepare();
                        MPfamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsolmayor7.isPlaying()){
                    MPsolmayor7.pause();
                    try {
                        MPsolmayor7.stop();
                        MPsolmayor7.prepare();
                        MPsolmayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                }else if (MPlamayor7.isPlaying()){
                    MPlamayor7.pause();
                    try {
                        MPlamayor7.stop();
                        MPlamayor7.prepare();
                        MPlamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }else if (MPsimayor7.isPlaying()){
                    MPsimayor7.pause();
                    try {
                        MPsimayor7.stop();
                        MPsimayor7.prepare();
                        MPsimayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;


            case R.id.BTNFamayorSiete:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPfamayor7.start();
                todomayorsiete.setImageResource(R.drawable.famayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.button_adivina);
                remayorsiete.setBackgroundResource(R.drawable.button_adivina);
                mimayorsiete.setBackgroundResource(R.drawable.button_adivina);
                famayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                solmayorsiete.setBackgroundResource(R.drawable.button_adivina);
                lamayorsiete.setBackgroundResource(R.drawable.button_adivina);
                simayorsiete.setBackgroundResource(R.drawable.button_adivina);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do mayor 7", "re mayor 7" o "mi mayor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomayor7.stop();
                try {
                    MPdomayor7.stop();
                    MPdomayor7.prepare();
                    MPdomayor7.seekTo(0);
                }catch (IOException q){
                    q.printStackTrace();

                }

                MPremayor7.stop();
                try {
                    MPremayor7.stop();
                    MPremayor7.prepare();
                    MPremayor7.seekTo(0);
                }catch (IOException rq){
                    rq.printStackTrace();
                }

                MPmimayor7.stop();
                try {
                    MPmimayor7.stop();
                    MPmimayor7.prepare();
                    MPmimayor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                // Si está sonando cualquier nota musical, entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                // Aplica para todas las condiciones de este botón.
                if (MPsolmayor7.isPlaying()) {
                    // Si está sonando el acorde musical "sol mayor 7" entonces se pausa.
                    MPsolmayor7.pause();
                    try {
                        //El acorde musical "sol mayor 7" se devuelve a su estado inical (se inicia desde cero).
                        MPsolmayor7.stop();
                        MPsolmayor7.prepare();
                        MPsolmayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPlamayor7.isPlaying()){
                    MPlamayor7.pause();
                    try {
                        MPlamayor7.stop();
                        MPlamayor7.prepare();
                        MPlamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                } else if (MPsimayor7.isPlaying()){
                    MPsimayor7.pause();
                    try {
                        MPsimayor7.stop();
                        MPsimayor7.prepare();
                        MPsimayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                break;



            case  R.id.BTNSolayorSiete:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPsolmayor7.start();
                todomayorsiete.setImageResource(R.drawable.solmayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.button_adivina);
                remayorsiete.setBackgroundResource(R.drawable.button_adivina);
                mimayorsiete.setBackgroundResource(R.drawable.button_adivina);
                famayorsiete.setBackgroundResource(R.drawable.button_adivina);
                solmayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                lamayorsiete.setBackgroundResource(R.drawable.button_adivina);
                simayorsiete.setBackgroundResource(R.drawable.button_adivina);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do mayor 7", "re mayor 7", "mi mayor 7" o "fa mayor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomayor7.stop();
                try {
                    MPdomayor7.stop();
                    MPdomayor7.prepare();
                    MPdomayor7.seekTo(0);
                }catch (IOException w){
                    w.printStackTrace();

                }

                MPremayor7.stop();
                try {
                    MPremayor7.stop();
                    MPremayor7.prepare();
                    MPremayor7.seekTo(0);
                }catch (IOException wq){
                    wq.printStackTrace();
                }


                MPmimayor7.stop();
                try {
                    MPmimayor7.stop();
                    MPmimayor7.prepare();
                    MPmimayor7.seekTo(0);
                }catch (IOException wp){
                    wp.printStackTrace();
                }

                MPfamayor7.stop();
                try {
                    MPfamayor7.stop();
                    MPfamayor7.prepare();
                    MPfamayor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                // Si está sonando la nota musical "la mayor 7", entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                if (MPlamayor7.isPlaying()){
                    MPlamayor7.stop();
                    try {
                        MPlamayor7.stop();
                        MPlamayor7.prepare();
                        MPlamayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                    // Si está sonando la nota musical "si mayor 7", entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                } else if (MPsimayor7.isPlaying()){
                    MPsimayor7.stop();
                    try {
                        MPsimayor7.stop();
                        MPsimayor7.prepare();
                        MPsimayor7.seekTo(0);
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }



                break;
            case R.id.BTNLamayorSiete:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPlamayor7.start();
                todomayorsiete.setImageResource(R.drawable.lamayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.button_adivina);
                remayorsiete.setBackgroundResource(R.drawable.button_adivina);
                mimayorsiete.setBackgroundResource(R.drawable.button_adivina);
                famayorsiete.setBackgroundResource(R.drawable.button_adivina);
                solmayorsiete.setBackgroundResource(R.drawable.button_adivina);
                lamayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                simayorsiete.setBackgroundResource(R.drawable.button_adivina);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);

                // Si están sonando una de estas notas musicales "do mayor 7", "re mayor 7", "mi mayor 7", "fa mayor 7" o "sol mayor 7", entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomayor7.stop();
                try {
                    MPdomayor7.stop();
                    MPdomayor7.prepare();
                    MPdomayor7.seekTo(0);
                }catch (IOException ttt){
                    ttt.printStackTrace();

                }
                MPremayor7.stop();
                try {
                    MPremayor7.stop();
                    MPremayor7.prepare();
                    MPremayor7.seekTo(0);
                }catch (IOException tqq){
                    tqq.printStackTrace();
                }

                MPmimayor7.stop();
                try {
                    MPmimayor7.stop();
                    MPmimayor7.prepare();
                    MPmimayor7.seekTo(0);
                }catch (IOException rww){
                    rww.printStackTrace();
                }


                MPfamayor7.stop();
                try {
                    MPfamayor7.stop();
                    MPfamayor7.prepare();
                    MPfamayor7.seekTo(0);
                }catch (IOException bhh){
                    bhh.printStackTrace();
                }

                MPsolmayor7.stop();
                try {
                    MPsolmayor7.stop();
                    MPsolmayor7.prepare();
                    MPsolmayor7.seekTo(0);
                }catch (IOException fff){
                    fff.printStackTrace();
                }


                // Si está sonando la nota musical "si mayor 7", entonces se detiene y da paso a que se escuche el botón actualmente seleccionado.
                if (MPsimayor7.isPlaying()){
                    // Si está sonando el acorde musical "si mayor 7" entonces se pausa.
                    MPsimayor7.stop();
                    try {
                        //El acorde musical "si mayor 7" se devuelve a su estado inical (se inicia desde cero).
                        MPsimayor7.stop();
                        MPsimayor7.prepare();
                        MPsimayor7.seekTo(0);
                    }catch (IOException xrr){
                        xrr.printStackTrace();
                    }
                }
                break;


            case R.id.BTNSimayorSiete:
                // Se reproduce la nota musical y se muestra la imagen de la nota.
                MPsimayor7.start();
                todomayorsiete.setImageResource(R.drawable.simayor7);

                // Cada vez que presionamos un botón sus bordes cambiaran a color rojo.
                domayorsiete.setBackgroundResource(R.drawable.button_adivina);
                remayorsiete.setBackgroundResource(R.drawable.button_adivina);
                mimayorsiete.setBackgroundResource(R.drawable.button_adivina);
                famayorsiete.setBackgroundResource(R.drawable.button_adivina);
                solmayorsiete.setBackgroundResource(R.drawable.button_adivina);
                lamayorsiete.setBackgroundResource(R.drawable.button_adivina);
                simayorsiete.setBackgroundResource(R.drawable.borde_rojo_bg);
                todomayorsiete.setBackgroundResource(R.drawable.button_adivina);


                // Si están sonando una de estas notas musicales "do mayor 7", "re mayor 7", "mi mayor 7", "fa mayor 7", "sol mayor 7" o "la mayor 7",
                // entonces se detienen  y dan paso a que se escuche el botón actualmente seleccionado.
                // Y el acorde musical que se detuvo se devuelve a su estado inical (se inicia desde cero).
                MPdomayor7.stop();
                try {
                    MPdomayor7.stop();
                    MPdomayor7.prepare();
                    MPdomayor7.seekTo(0);
                }catch (IOException t){
                    t.printStackTrace();

                }

                MPremayor7.stop();
                try {
                    MPremayor7.stop();
                    MPremayor7.prepare();
                    MPremayor7.seekTo(0);
                }catch (IOException tq){
                    tq.printStackTrace();
                }


                MPmimayor7.stop();
                try {
                    MPmimayor7.stop();
                    MPmimayor7.prepare();
                    MPmimayor7.seekTo(0);
                }catch (IOException rw){
                    rw.printStackTrace();
                }

                MPfamayor7.stop();
                try {
                    MPfamayor7.stop();
                    MPfamayor7.prepare();
                    MPfamayor7.seekTo(0);
                }catch (IOException bh){
                    bh.printStackTrace();
                }

                MPsolmayor7.stop();
                try {
                    MPsolmayor7.stop();
                    MPsolmayor7.prepare();
                    MPsolmayor7.seekTo(0);
                }catch (IOException pr){
                    pr.printStackTrace();
                }

                MPlamayor7.stop();
                try {
                    MPlamayor7.stop();
                    MPlamayor7.prepare();
                    MPlamayor7.seekTo(0);
                }catch (IOException ju){
                    ju.printStackTrace();
                }
                break;
        }

    }
}

