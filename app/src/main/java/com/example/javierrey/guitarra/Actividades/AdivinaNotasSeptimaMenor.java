package com.example.javierrey.guitarra.Actividades;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerCinco;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerCuatro;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerDos;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerSeis;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerSiete;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerTres;
import com.example.javierrey.guitarra.Adapter.AdapterSpinnerUno;
import com.example.javierrey.guitarra.Items.SpinnerItenCincoMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenCuatroMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenDosMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenSeisMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenSieteMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenTresMenor;
import com.example.javierrey.guitarra.Items.SpinnerItenUnoMenor;
import com.example.javierrey.guitarra.R;
import java.util.ArrayList;
/* Implementamos la  interfaz OnClickListener y hacemos las importaciones de la interfaz*/
public class AdivinaNotasSeptimaMenor extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout LLunomenor, LLdosmenor, LLtresmenor, LLcuatromenor, LLcincomenor, LLseismenor, LLsietemenor;

    /* Necesitamos una lista que deseamos que muestre el spinner. Agarramos los métodos de la clase creada (SpinnerItenUnoMenor)
    también llamamos a esta clase los métodos de la clase AdapterSpinnerUno para poder implementarlos.
    Asi mismo con los demás...
    */
    private ArrayList<SpinnerItenUnoMenor> spinnerItenUnoMenors;
    private AdapterSpinnerUno adapterSpinnerUno;

    private ArrayList<SpinnerItenDosMenor> spinnerItenDosMenors;
    private AdapterSpinnerDos adapterSpinnerDos;

    private ArrayList<SpinnerItenTresMenor> spinnerItenTresMenors;
    private AdapterSpinnerTres adapterSpinnerTres;

    private ArrayList<SpinnerItenCuatroMenor> spinnerItenCuatroMenors;
    private AdapterSpinnerCuatro adapterSpinnerCuatro;

    private ArrayList<SpinnerItenCincoMenor> spinnerItenCincoMenors;
    private AdapterSpinnerCinco adapterSpinnerCinco;

    private ArrayList<SpinnerItenSeisMenor> spinnerItenSeisMenors;
    private AdapterSpinnerSeis adapterSpinnerSeis;

    private ArrayList<SpinnerItenSieteMenor> spinnerItenSieteMenors;
    private AdapterSpinnerSiete adapterSpinnerSiete;

    /* Instanciamos cada objeto de la clase privada Spinner */
    private Spinner spinnerUnoMenor, spinnerDosMenor, spinnerTresMenor, spinnerCuatroMenor,
            spinnerCincoMenor, spinnerSeisMenor, spinnerSieteMenor;

    /* Instanciamos cada objeto de la clase privada Imageview y MediaPlayer */
    private ImageView unoMenor, dosMenor, tresMenor, cuatroMenor, cincoMenor, seisMenor, sieteMenor;
    private MediaPlayer MPunoMenor, MPdosMenor, MPtresMenor, MPcuatroMenor, MPcincoMenor, MPseisMenor, MPsieteMenor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adivina_notas_septima_menor);
        setTitle(getString(R.string.escuchayelije));


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        LLunomenor = (LinearLayout)findViewById(R.id.LLUnomayor);
        LLdosmenor = (LinearLayout)findViewById(R.id.LLDosmayor);
        LLtresmenor = (LinearLayout)findViewById(R.id.LLTresmayor);
        LLcuatromenor = (LinearLayout)findViewById(R.id.LLCuatromayor);
        LLcincomenor = (LinearLayout)findViewById(R.id.LLCincomayor);
        LLseismenor = (LinearLayout)findViewById(R.id.LLSeismayor);
        LLsietemenor = (LinearLayout)findViewById(R.id.LLSietemayor);

        /* A cada referencia de MediaPlayer la enlazamos con su id */
        MPunoMenor = MediaPlayer.create(this, R.raw.domenor7);
        MPdosMenor = MediaPlayer.create(this, R.raw.remenor7);
        MPtresMenor = MediaPlayer.create(this, R.raw.mimenor7);
        MPcuatroMenor = MediaPlayer.create(this, R.raw.famenor7);
        MPcincoMenor = MediaPlayer.create(this, R.raw.solmenor7);
        MPseisMenor = MediaPlayer.create(this, R.raw.lamenor7);
        MPsieteMenor = MediaPlayer.create(this, R.raw.simenor7);

        /* A cada referencia de las ImageView las enlazamos con su id */
        unoMenor = (ImageView)findViewById(R.id.SPImagenUnoMenor);
        dosMenor = (ImageView)findViewById(R.id.SPImagenDosMenor);
        tresMenor = (ImageView)findViewById(R.id.SPImagenTresMenor);
        cuatroMenor = (ImageView)findViewById(R.id.SPImagenCuatroMenor);
        cincoMenor = (ImageView)findViewById(R.id.SPImagenCincoMenor);
        seisMenor = (ImageView)findViewById(R.id.SPImagenSeisMenor);
        sieteMenor = (ImageView)findViewById(R.id.SPImagenSieteMenor);

        //Asociamos el lístener a los botones.
        unoMenor.setOnClickListener(this);
        dosMenor.setOnClickListener(this);
        tresMenor.setOnClickListener(this);
        cuatroMenor.setOnClickListener(this);
        cincoMenor.setOnClickListener(this);
        seisMenor.setOnClickListener(this);
        sieteMenor.setOnClickListener(this);

        LLunomenor.setOnClickListener(this);
        LLdosmenor.setOnClickListener(this);
        LLtresmenor.setOnClickListener(this);
        LLcuatromenor.setOnClickListener(this);
        LLcincomenor.setOnClickListener(this);
        LLseismenor.setOnClickListener(this);

        SpinnerUnoMenorPersonalizado();
        SpinnerDosMenorPersonalizado();
        SpinnerTresMenorPersonalizado();
        SpinnerCuatroMenorPersonalizado();
        SpinnerCincoMenorPersonalizado();
        SpinnerSeisMenorPersonalizado();
        SpinnerSieteMenorPersonalizado();

        /* A cada refrencia de los spinner lo enlazamos con su id */
        spinnerUnoMenor = (Spinner) findViewById(R.id.SPUnoMenor);
        spinnerDosMenor = (Spinner)findViewById(R.id.SPDosMenor);
        spinnerTresMenor = (Spinner)findViewById(R.id.SPTresMenor);
        spinnerCuatroMenor = (Spinner)findViewById(R.id.SPCuatroMenor);
        spinnerCincoMenor = (Spinner)findViewById(R.id.SPCincoMenor);
        spinnerSeisMenor = (Spinner)findViewById(R.id.SPSeisMenor);
        spinnerSieteMenor = (Spinner)findViewById(R.id.SPSieteMenor);


        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerUnoMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenUnoMenor clickedItem = (SpinnerItenUnoMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemMemor();
               /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.simenor7))){
                    LLunomenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLunomenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerDosMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenDosMenor clickedItem = (SpinnerItenDosMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemDosMemor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.lamenor7))){
                    LLdosmenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLdosmenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerTresMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenTresMenor clickedItem = (SpinnerItenTresMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemTresMemor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.solmenor7))){
                    LLtresmenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLtresmenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCuatroMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCuatroMenor clickedItem = (SpinnerItenCuatroMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCuatroMemor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.famenor7))){
                    LLcuatromenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLcuatromenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerCincoMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenCincoMenor clickedItem = (SpinnerItenCincoMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemCincoMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.mimenor7))){
                    LLcincomenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLcincomenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSeisMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSeisMenor clickedItem = (SpinnerItenSeisMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSeisMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.remenor7))){
                    LLseismenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLseismenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /* Con el método "setOnItemSelectedListener" especificamos que elementos se seleccionan en el spinner */
        spinnerSieteMenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerItenSieteMenor clickedItem = (SpinnerItenSieteMenor) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getNombreItemSieteMenor();
                /* Condición para comprobar cuál es el item correcto en el spinner */
                if (clickedCountryName.equals(getString(R.string.domenor7))){
                    LLsietemenor.setBackgroundResource(R.drawable.degradadoadivinabordenuevo);
                    Correcto();
                }else if (clickedCountryName.equals(getString(R.string.opcion))){

                }else {
                    LLsietemenor.setBackgroundResource(R.drawable.degradadoadivina);
                    InCorrecto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* Asignados el origen de los datos desde los recursos. */
        adapterSpinnerUno = new AdapterSpinnerUno(this, spinnerItenUnoMenors);
        adapterSpinnerDos = new AdapterSpinnerDos(this, spinnerItenDosMenors);
        adapterSpinnerTres = new AdapterSpinnerTres(this, spinnerItenTresMenors);
        adapterSpinnerCuatro = new AdapterSpinnerCuatro(this, spinnerItenCuatroMenors);
        adapterSpinnerCinco = new AdapterSpinnerCinco(this, spinnerItenCincoMenors);
        adapterSpinnerSeis = new AdapterSpinnerSeis(this, spinnerItenSeisMenors);
        adapterSpinnerSiete = new AdapterSpinnerSiete(this, spinnerItenSieteMenors);


    /* Seteamos el adaptador */
        spinnerUnoMenor.setAdapter(adapterSpinnerUno);
        spinnerDosMenor.setAdapter(adapterSpinnerDos);
        spinnerTresMenor.setAdapter(adapterSpinnerTres);
        spinnerCuatroMenor.setAdapter(adapterSpinnerCuatro);
        spinnerCincoMenor.setAdapter(adapterSpinnerCinco);
        spinnerSeisMenor.setAdapter(adapterSpinnerSeis);
        spinnerSieteMenor.setAdapter(adapterSpinnerSiete);
    }

    /* Creamos un arreglo para el septimo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSieteMenorPersonalizado() {
        spinnerItenSieteMenors = new ArrayList<>();
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenSieteMenors.add(new SpinnerItenSieteMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el sexto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerSeisMenorPersonalizado() {
        spinnerItenSeisMenors = new ArrayList<>();
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenSeisMenors.add(new SpinnerItenSeisMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el quinto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCincoMenorPersonalizado() {
        spinnerItenCincoMenors = new ArrayList<>();
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenCincoMenors.add(new SpinnerItenCincoMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el cuarto spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerCuatroMenorPersonalizado() {
        spinnerItenCuatroMenors = new ArrayList<>();
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenCuatroMenors.add(new SpinnerItenCuatroMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el tercer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerTresMenorPersonalizado() {
        spinnerItenTresMenors = new ArrayList<>();
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenTresMenors.add(new SpinnerItenTresMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el segundo spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerDosMenorPersonalizado() {
        spinnerItenDosMenors = new ArrayList<>();
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
        spinnerItenDosMenors.add(new SpinnerItenDosMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
    }

    /* Creamos un arreglo para el primer spinner con las opciones e indicamos el contenidos del mismo  */
    private void SpinnerUnoMenorPersonalizado() {
        spinnerItenUnoMenors = new ArrayList<>();
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.opcion), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.domenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.mimenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.lamenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.solmenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.remenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.simenor7), R.drawable.ic_nota_dp));
        spinnerItenUnoMenors.add(new SpinnerItenUnoMenor(getString(R.string.famenor7), R.drawable.ic_nota_dp));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /* Se reproduce el sonido del primer botón */
            case R.id.SPImagenUnoMenor:
                MPsieteMenor.start();
                break;

            /* Se reproduce el sonido del segundo botón */
            case R.id.SPImagenDosMenor:
                MPseisMenor.start();
                break;

            /* Se reproduce el sonido del tercer botón */
            case R.id.SPImagenTresMenor:
                MPcincoMenor.start();
                break;

            /* Se reproduce el sonido del cuarto botón */
            case R.id.SPImagenCuatroMenor:
                MPcuatroMenor.start();
                break;

            /* Se reproduce el sonido del quinto botón */
            case  R.id.SPImagenCincoMenor:
                MPtresMenor.start();
                break;

            /* Se reproduce el sonido del sexto botón */
            case R.id.SPImagenSeisMenor:
                MPdosMenor.start();
                break;

            /* Se reproduce el sonido del septimo botón */
            case R.id.SPImagenSieteMenor:
                MPunoMenor.start();
                break;
        }
    }

    /* Método ""InCorreto para mostrar que está incorrecto, es llama cada vez que no se cumplen las condiciones en los botones*/
    private void InCorrecto(){

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.incorrecto));
        imageView.setImageResource(R.drawable.ic_marca_de_cancelar);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /* Método "Correto" para mostrar que está correcto, es llama cada vez que se cumplen las condiciones en los botones*/
    private void Correcto(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.L));

        TextView text = (TextView) layout.findViewById(R.id.text);
        ImageView imageView = (ImageView) layout.findViewById(R.id.ToastImg);
        text.setText(getString(R.string.correcto));
        imageView.setImageResource(R.drawable.ic_correcto_simbolo);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        int id = item.getItemId();
        if (id == android.R.id.home){
            startActivity(new Intent(this, Juegos.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}